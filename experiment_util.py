"""This file contains everything related to the experiment config: a class
that stores all parameters for defining an experiment and functions for
parsing config files.
"""

import sys
import os
import time

import constants


class Experiment:
    """Defines an experiment by all its parameters.
    
    Intended to be used as input for the run_experiment() function.
    
    Attributes:
        name: a str representing the name of the experiment
        duration: an integer indicating how long the experiment will run in
          seconds
        number_senders: an integers defining the number of server that act
          as senders
        bandwidth: a string defining the bottleneck bandwidth and the unit
          in the format used by netem, e.g. "100kbit" or "10gbit"
        queue_size: an integer defining the bottleneck queue (buffer) size
          in packets
        qdisc: a string defining the qdisc to set at the bottleneck
        loss: a list of floats where each element defines the percentage of
          induced packet loss for one sender
        rtts: a list of integers where each element defines the RTT of one
          sender in ms (as used by netem)
        jitters: a list of integers where each element defines the jitter
          for one sender in ms (as used by netem)
        congestion_control_algorithms: a list of strings where each element
          defines the congestion control algorithm for one sender
        initial_windows: a list of integers where each element defines the
          initial window value for one sender
        flow_sizes_per_sender: a list of lists that contains for each
          sender a list of flow sizes in packets for generating random
          flows during the experiment
        flow_sizes_files: a list of strings that tell the files from which
          the flow_sizes_per_sender were obtained
        flow_interarrival_times_per_sender: a list of lists that contains
          for each sender a list of flow interarrival times in us for
          generating random flows during the experiment
        flow_interarrival_times_files: a list of strings that tell the files
          from which the flow_interarrival_times_per_sender were obtained
        time_scaling_factor: an integer by which the flow interarrival
          times will be multiplied, e.g. for scaling us to ms for a more
          gentle sending process
        random_seed: a float that specifies a seed for the random flow
          generation processes; can be used to repeat a certain experiment
          with the exact same flow sizes and interarrival times. If None,
          the system time will be used as random seed
        force_flows: a list of lists of (float, int) tuples that contains
          for each sender a list of flows that should be enforced, where the
          enforced flow is defined through (start_time, size), i.e., it
          begins at start_time (in s after the experiment start) and has a
          size of size packets
        udp_background_traffic: a list of strings that define for each
          sender how much UDP background traffic it should send, in the
          format used by iperf3, e.g. "100k" or "15m"
        rate_limits: a list of strings that define for each sender the
          sending rate it gets limited to, in the format used by netem,
          e.g. "8mbit"
        quickack: a bool indicating whether the receiver should enable
          TCP_QUICKACK (i.e., disable delayed ACKs) or not (only for fctm
          executable)
        executables: a list of strings that define for each sender which
          executable it should use for traffic generation; this can be
          either "fctm" or "pcp. If fctm, the sender uses fctm to send
          data, and the congestion control algorithm of this sender is set
          using sysctl. If pcp, the sender uses an executable in the user
          space PCP code, and the congestion control algorithm of this
          sender defines the name of the executable to use
        slot_mins: a list of integers where each element defines the
          minimum slot time for the ACKs that return to one sender in ms
          (as used by netem's slot parameter)
        slot_maxs: a list of integers where each element defines the
          maximum slot time for the ACKs that return to one sender in ms
          (as used by netem's slot parameter)
    """

    def __init__(
            self,
            name: str,
            duration: int,
            number_senders: int,
            bandwidth: str,
            queue_size: int,
            qdisc: str,
            loss: list,
            rtts: list,
            jitters: list,
            congestion_control_algorithms: list,
            initial_windows: list,
            flow_sizes_per_sender: list,
            flow_sizes_files: list,
            flow_interarrival_times_per_sender: list,
            flow_interarrival_times_files: list,
            time_scaling_factor: int,
            random_seed: float,
            force_flows: list,
            udp_background_traffic: list,
            rate_limits: list,
            quickack: bool,
            executables: list,
            slot_mins: list,
            slot_maxs: list,
            ):
        self.name = name
        self.duration = duration
        self.number_senders = number_senders
        self.bandwidth = bandwidth
        self.queue_size = queue_size
        self.qdisc = qdisc
        self.loss = loss
        self.rtts = rtts
        self.jitters = jitters
        self.congestion_control_algorithms = congestion_control_algorithms
        self.initial_windows = initial_windows
        self.flow_sizes_per_sender = flow_sizes_per_sender
        self.flow_sizes_files = flow_sizes_files
        self.flow_interarrival_times_per_sender = \
                flow_interarrival_times_per_sender
        self.flow_interarrival_times_files = flow_interarrival_times_files
        self.time_scaling_factor = time_scaling_factor
        self.random_seed = random_seed if random_seed else time.time()
        self.force_flows = force_flows
        self.udp_background_traffic = udp_background_traffic
        self.rate_limits = rate_limits
        self.quickack = quickack
        self.executables = executables
        self.slot_mins = slot_mins
        self.slot_maxs = slot_maxs

        # Sanity checks
        if len(rtts) != number_senders:
            print('Illegal config: the number of RTT values provided must ' \
                    'be equal to number_senders.')
            sys.exit(1)
        if jitters and len(jitters) != number_senders:
            print('Illegal config: the number of jitter values provided must ' \
                    'be equal to number_senders.')
            print('If only some sender should experience jitter, please ' \
                    'provide 0 for the other senders.')
            sys.exit(1)
        if loss and len(loss) != number_senders:
            print('Illegal config: the number of loss values provided must ' \
                    'be equal to number_senders.')
            print('If only some sender should experience loss, please ' \
                    'provide 0 for the other senders.')
            sys.exit(1)
        if initial_windows and len(initial_windows) != number_senders:
            print('Illegal config: the number of initial window values ' \
                    'provided must be equal to number_senders.')
            print('If only some sender should use a non-standard IW, please ' \
                    'provide 10 as default for the other senders.')
            sys.exit(1)
        if len(congestion_control_algorithms) != number_senders:
            print('Illegal config: the number of congestion control ' \
                    'algorithms provided must be equal to number_senders.')
            sys.exit(1)
        if time_scaling_factor <= 0:
            print('Illegal config: time scaling factor must be > 0.')
            sys.exit(1)
        if slot_maxs and not slot_mins:
            print('Illegal config: can only use slot_max when there is a ' \
                    'slot_min.')
            sys.exit(1)

    def __repr__(self):
        return 'name: {}\n'.format(self.name) + \
                'duration: {}\n'.format(self.duration) + \
                'number_senders: {}\n'.format(self.number_senders) + \
                'bandwidth: {}\n'.format(self.bandwidth) + \
                'queue_size: {}\n'.format(self.queue_size) + \
                'qdisc: {}\n'.format(self.qdisc) + \
                'loss: {}\n'.format(self.loss) + \
                'rtts: {}\n'.format(self.rtts) + \
                'jitters: {}\n'.format(self.jitters) + \
                'congestion_control_algorithms: {}\n'.format(
                        self.congestion_control_algorithms) + \
                'initial_windows: {}\n'.format(self.initial_windows) + \
                'flow sizes from: {}\n'.format(self.flow_sizes_files) + \
                'flow interarrival times from: {}\n'.format(
                        self.flow_interarrival_times_files) + \
                'time_scaling_factor: {}\n'.format(self.time_scaling_factor) + \
                'random seed: {}\n'.format(self.random_seed) + \
                'force flows: {}\n'.format(self.force_flows) + \
                'udp_background_traffic: {}\n'.format(
                        self.udp_background_traffic) + \
                'rate limits: {}\n'.format(self.rate_limits) + \
                'quickack: {}\n'.format(self.quickack) + \
                'executables: {}\n'.format(self.executables) + \
                'slot_mins: {}\n'.format(self.slot_mins) + \
                'slot_maxs: {}\n'.format(self.slot_maxs)


def parse_line(line: str) -> (str, str):
    """Parse one line of a config file.

    Args:
        line: the whole line as string

    Returns:
        a tuple of two strings: the first one is the variable (e.g.,
          "number_senders", "bandwidth", "rtts", ...) and the second one
          is its value (e.g., "4", "100mbit", "20, 30, 40, 50", ...)
    """

    line = line.strip()

    split = line.split(':')
    variable = split[0].strip()
    value = split[1].strip()

    return variable, value


def parse_bandwidth(bandwidth: str, return_iperf3_style: bool = False) -> str:
    """Parse the bandwidth string as given in a config file and return a
    string that can be used by netem, e.g. "100kbit".

    Args:
        bandwidth: a string that defines the bandwidth value from the
          config file
        return_iperf3_style: if True, return the bandwidth string in the
          format used by iperf3 not the format used by netem, e.g. "15m"

    Returns:
        a string that defines the bandwidth and can be used by netem, i.e.
          with units like "kbit" (or if return_iperf3_style, the string is
          such that it can be used by iperf3)
    """

    if bandwidth.isdigit():
        unit = 'm'
        bw_str = bandwidth
    else:
        if bandwidth.endswith(('k', 'kbit', 'kbps')):
            unit = 'k'
        elif bandwidth.endswith(('m', 'mbit', 'mbps')):
            unit = 'm'
        elif bandwidth.endswith(('g', 'gbit', 'gbps')):
            unit = 'g'
        bw_str = bandwidth.split(unit)[0]

    unit_str = unit if return_iperf3_style else unit + 'bit'
    return bw_str + unit_str


def bandwidth_str_to_int(bandwidth_str: str) -> int:
    """Parse a bandwidth string as stored e.g. in the Experiment class and
    convert it to an integer in mbit/s.

    Args:
        bandwidth_str: a string representation of a bandwidth value; this
          can be the format of netem or iperf3

    Returns:
        an integer representing the bandwidth in mbit/s
    """

    if not bandwidth_str:
        return None

    if bandwidth_str.endswith(('k', 'kbit', 'kbps')):
        unit = 'k'
        factor = 1 / 1000
    elif bandwidth_str.endswith(('m', 'mbit', 'mbps')):
        unit = 'm'
        factor = 1
    elif bandwidth_str.endswith(('g', 'gbit', 'gbps')):
        unit = 'g'
        factor = 1000

    bandwidth = bandwidth_str.split(unit)[0]
    bandwidth_mbit = int(bandwidth) * factor

    return bandwidth_mbit


def parse_queue_size(queue_size_str: str, bandwidth_str: int) -> int:
    """Parse the queue size string as given in a config file and return an
    integer that represents the queue size in packets. The string could be
    the queue size in packets or in ms, in which case this function
    converts the value to packets.

    Args:
        queue_size_str: a string that defines the queue size either in
          packets or in ms
        bandwidth_str: a string representation of a bandwidth value; this
          can be None if the queue size is specified in packets but must
          be a valid bandwidth string if the queue size is specified in ms

    Returns:
        an integer representing the queue size in packets
    """

    if queue_size_str.isdigit() or queue_size_str.endswith('p'):
        return int(queue_size_str.replace('p', ''))
    elif queue_size_str.endswith('ms'):
        queue_size_ms = int(queue_size_str[:-2])
        bandwidth_mbit_per_s = bandwidth_str_to_int(bandwidth_str)

        bytes_per_packet = \
                constants.TCP_PACKET_SIZE + constants.TCP_HEADER_SIZE + \
                constants.IP_HEADER_SIZE

        bandwidth_bit_per_s = bandwidth_mbit_per_s * constants.MBITS_TO_BITS
        bandwidth_byte_per_s = bandwidth_bit_per_s / constants.BITS_PER_BYTE
        bandwidth_pkt_per_s = bandwidth_byte_per_s / bytes_per_packet
        bandwidth_pkt_per_ms = \
                bandwidth_pkt_per_s * constants.MILLISECONDS_TO_SECONDS

        return round(bandwidth_pkt_per_ms * queue_size_ms)


def read_lines_of_dataset_file(input_file: str) -> list:
    """Read the contents of a file line by line and return the contents as a
    list of integers.

    The file could be a flow sizes or a flow interarrival times dataset.

    Args:
        input_file: the path to the file you want to read

    Returns:
        a list[int] containing the lines of the file
    """

    with open(input_file) as f:
        return [int(line.strip()) for line in f.readlines()
                if line.strip() != '']


def parse_flow_dataset(input_str: str, number_senders: int) -> list:
    """Parse the flow sizes or flow interarrival times string as given in a
    config file and return a list of lists that contains for each sender the
    dataset it should use.

    Args:
        input_str: a string pointing to one or more files that contain the
          dataset; if one file is provided, the file is used for all
          senders, otherwise there must be one file per sender
        number_senders: the number of senders in the experiment

    Returns:
        a list of lists that contains for each sender the list of flow sizes
          or flow interarrival times it uses
    """

    split = input_str.split(',')

    if len(split) == 1:  # One file for all senders
        dataset_file = split[0].strip()
        data = read_lines_of_dataset_file(dataset_file)
        dataset_per_sender = [data] * number_senders
    else:  # One file per sender
        dataset_per_sender = []
        for dataset_file in split:
            dataset_file = dataset_file.strip()
            data = read_lines_of_dataset_file(dataset_file)
            dataset_per_sender.append(data)

    return dataset_per_sender


def is_number(string: str) -> bool:
    """Check if a string contains a number, i.e., an int or a float."""

    return string.replace('.','',1).isdigit()


def parse_force_flows(input_str: str) -> list:
    """Parse the force flows string as given in a config file and return a
    list of lists that contains for each sender a list of flows to force.

    Args:
        input_str: a string defining the flows to enforce; this should be a
          comma-separated list of space-separated flow start times and
          sizes, e.g., "1 400 4 2000, 0" enforces sender 0 to send a 400
          packets flow after 1 sec and a 2000 packets flow after 4 sec, and
          sender 1 does not enforce any flows

    Returns:
        a list of lists of tuples that contains for each sender a list of
          flow to enforce, and each flow to enforce is defined by a (float,
          int) tuple that contains the flow start time and its size, e.g.,
          [[(1.0, 400), (4.0, 2000)], None] for the above input_str example
    """

    split = input_str.split(',')

    force_flows = []
    for i, sender in enumerate(split):  # Iterate senders
        flows = sender.split()
        if len(flows) <= 1:
            force_flows.append(None)
        else:
            force_flows.append([])
            cur_time = 0.0
            cur_size = 0
            parsing_time = True  # if True, parsing time, else parsing size

            for f in flows:  # Iterate flows of sender
                if parsing_time:
                    cur_time = float(f)
                else:
                    cur_size = int(f)
                    force_flows[i].append((cur_time, cur_size))
                parsing_time = not parsing_time

    return force_flows


def parse_list_of_bandwidth_values(
        input_str: str,
        iperf3_style: bool = False,
        ) -> list:
    """Parse a list of bandwidth values such as a list of UDP background
    traffic values or rate limits and return a list of string that define
    for each sender the bandwidth value.

    Args:
        input_str: a string defining the list of bandwidth values; this
          should be a comma-separated list of bandwidth values that can be
          parsed by parse_bandwidth(), or "0" if the value should not be
          used at all
        iperf3_style: a bool indicating whether parse_bandwidth() should
          return iperf3-style bandwidth strings (default: netem-style)

    Returns:
        a list of strings that contain the  bandwidth values, or None to
          indicate they should not be used (i.e., no UDP or no rate limits)
    """

    if input_str.strip() == '0':
        bw_values = None
    else:
        bw_values = []
        for bw in input_str.split(','):
            bw_values.append(parse_bandwidth(bw.strip(),
                    return_iperf3_style=iperf3_style)
                    if bw.strip() != '0' else None)

    return bw_values


def from_config(config_file: str) -> Experiment:
    """Parse a config file and return an instance of Experiment that
    represents this config.

    Args:
        config_file: a string representing the path to the config file

    Returns:
        an instance of Experiment that represents the config from the file
    """

    # Initialize variables
    name = os.path.basename(config_file)  # name is just the config file name
    duration = None
    number_senders = None
    bandwidth = None  # default: don't set bw, use physical link bw
    queue_size = None
    qdisc = constants.DEFAULT_QDISC  # default: FIFO queue
    loss = None  # default: no loss
    rtts = None
    jitters = None  # default: no jitter
    congestion_control_algorithms = None
    initial_windows = None  # default: do not set IW and use IW=10
    flow_sizes_per_sender = None
    flow_sizes_files = None
    flow_interarrival_times_per_sender = None
    flow_interarrival_times_files = None
    time_scaling_factor = 1  # default: don't scale the values
    random_seed = None  # default: generate a new random seed
    force_flows = None  # default: do not force any flows
    udp_background_traffic = None  # default: no background traffic
    rate_limits = None  # default: no rate limits
    quickack = False  # default: disable TCP_QUICKACK (enable delayed ACKs)
    executables = None  # default: fctm
    slot_mins = None  # default: no slotting
    slot_maxs = None  # default: no slotting max

    with open(config_file) as f:
        lines = f.readlines()

        # First pass: obtain number of senders
        # because this value is needed for reading the dataset values (i.e. the
        # flow sizes and flow interarrival times)
        for line in lines:
            if line.startswith('#') or line.strip() == '':  # comment or empty
                continue

            variable, value = parse_line(line)

            if variable in ('number_senders', 'n'):
                number_senders = int(value)

        # Second pass: obtain all other values
        for line in lines:
            if line.startswith('#') or line.strip() == '':  # comment or empty
                continue

            variable, value = parse_line(line)

            if variable in ('duration', 'd'):
                duration = int(value)
            elif variable in ('bandwidth', 'b'):
                bandwidth = parse_bandwidth(value)
            elif variable in ('queue_size', 'q'):
                queue_size = parse_queue_size(value, bandwidth)
            elif variable in ('qdisc', 'Q'):
                qdisc = value
            elif variable in ('loss', 'l'):
                loss = [float(lo.strip()) for lo in value.split(',')]
            elif variable in ('rtts', 'r'):
                rtts = [int(rtt.strip()) for rtt in value.split(',')]
            elif variable in ('jitters', 'j'):
                jitters = [int(jitter.strip()) for jitter in value.split(',')]
            elif variable in ('congestion_control_algorithms', 'ccas', 'c'):
                congestion_control_algorithms = [cca.strip()
                        for cca in value.split(',')]
            elif variable in ('initial_windows', 'w'):
                initial_windows = [int(iw.strip()) for iw in value.split(',')]
            elif variable in ('flow_sizes_per_sender', 's'):
                flow_sizes_per_sender = parse_flow_dataset(value,
                        number_senders)
                flow_sizes_files = value
            elif variable in ('flow_interarrival_times_per_sender', 'i'):
                flow_interarrival_times_per_sender = parse_flow_dataset(value,
                        number_senders)
                flow_interarrival_times_files = value
            elif variable in ('time_scaling_factor', 't'):
                time_scaling_factor = int(value)
            elif variable in ('random_seed', 'R'):
                random_seed = float(value) if is_number(value) else None
            elif variable in ('force_flows', 'f'):
                force_flows = parse_force_flows(value)
            elif variable in ('udp_background_traffic', 'u'):
                udp_background_traffic = parse_list_of_bandwidth_values(value,
                        iperf3_style=True)
            elif variable in ('rate_limit', 'rate_limits', 'a'):
                rate_limits = parse_list_of_bandwidth_values(value,
                        iperf3_style=False)
            elif variable in ('quickack', 'A'):
                quickack = False if value in ('0', 'false', 'False') else True
            elif variable in ('executables', 'x'):
                executables = [exe.strip() for exe in value.split(',')]
            elif variable in ('slot_mins', 'S'):
                slot_mins = [int(slot.strip()) for slot in value.split(',')]
            elif variable in ('slot_maxs', 'Z'):
                slot_maxs = [int(slot.strip()) for slot in value.split(',')]

    if not (name and duration and number_senders and queue_size and rtts and
            congestion_control_algorithms and flow_sizes_per_sender and
            flow_interarrival_times_per_sender):
        print('Config file misses a mandatory argument! Cannot parse.')
        sys.exit(1)

    experiment = Experiment(
            name=name,
            duration=duration,
            number_senders=number_senders,
            bandwidth=bandwidth,
            queue_size=queue_size,
            qdisc=qdisc,
            loss=loss,
            rtts=rtts,
            jitters=jitters,
            congestion_control_algorithms=congestion_control_algorithms,
            initial_windows=initial_windows,
            flow_sizes_per_sender=flow_sizes_per_sender,
            flow_sizes_files=flow_sizes_files,
            flow_interarrival_times_per_sender=\
                    flow_interarrival_times_per_sender,
            flow_interarrival_times_files=flow_interarrival_times_files,
            time_scaling_factor=time_scaling_factor,
            random_seed=random_seed,
            force_flows=force_flows,
            udp_background_traffic=udp_background_traffic,
            rate_limits=rate_limits,
            quickack=quickack,
            executables=executables,
            slot_mins=slot_mins,
            slot_maxs=slot_maxs,
            )
    #print(experiment)

    return experiment

