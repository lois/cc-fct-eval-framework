#!/usr/bin/env python3

"""Script for performing an analysis of a result created by an experiment
(via main.py). This creates a slowdown summary and a flow summary plot (cf.
summarize_flows.py) and a metrics summary (cf. compute_metrics.py).
"""

import argparse
import os

import summarize_flows
import compute_metrics
import plot_buffer_log
import plot_packet_loss
import constants


def parse_args():
    """Parse command line arguments.

    Returns:
        the args Namespace from argparse
    """

    parser = argparse.ArgumentParser(description='Perform an analysis of a ' + \
            'completed experiment: compute the slowdown for all flows and ' + \
            'write them into a summary file and create a plot of the ' + \
            'buffer backlog.')

    parser.add_argument('directory', help='the path to the directory that ' + \
            'contains the experiment result files')

    return parser.parse_args()


def analyze(results_directory: str):
    """Analyze a completed experiment of which the results are stored in the
    provided results_directory.
    This generates a slowdown summary, a metrics summary, and a flow summary
    plot.
    
    Args:
        results_directory: the path to a directory that contains experiment
          result files
    """

    experiment_config_file = os.path.join(results_directory,
            constants.EXPERIMENT_CONFIG_FILE_NAME)

    # Summarize flows
    slowdown_summary_file = os.path.join(results_directory,
            constants.SLOWDOWN_SUMMARY_FILE_NAME)
    flow_summary_plot_file = os.path.join(results_directory,
            constants.FLOW_SUMMARY_PLOT_FILE_NAME)

    summarize_flows.summarize_flows_from_results_directory(results_directory,
            slowdown_summary_file, flow_summary_plot_file)

    # Metrics
    compute_metrics.write_metrics_to_file(results_directory,
            constants.METRICS_SUMMARY_FILE_NAME)

    # Plot buffer backlog and packet loss
    buffer_log_file = os.path.join(results_directory,
            constants.BUFFER_LOG_FILE_NAME)
    buffer_plot_file = os.path.join(results_directory,
            constants.BUFFER_PLOT_FILE_NAME)

    plot_buffer_log.plot_buffer_log_from_files(buffer_log_file,
            buffer_plot_file, experiment_config_file)

    loss_plot_file = os.path.join(results_directory,
            constants.LOSS_PLOT_FILE_NAME)

    plot_packet_loss.plot_packet_loss_from_files(buffer_log_file,
            loss_plot_file, experiment_config_file)


def main():
    args = parse_args()
    results_directory = args.directory

    analyze(results_directory)


if __name__ == '__main__':
    main()

