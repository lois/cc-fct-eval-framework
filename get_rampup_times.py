#!/usr/bin/env python3

"""Functions for getting the rampup times of flows, i.e., the time after
which a flow transitions from slow start to congestion avoidance.
"""

import argparse

import constants
from server_config import *


def parse_args():
    """Parse command line arguments.

    Returns:
        the args Namespace from argparse
    """

    parser = argparse.ArgumentParser(description='Parse an ss log file ' + \
            'and print the rampup times of all flows, i.e., the times ' + \
            'after which the flows transition from slow start to ' + \
            'congestion avoidance.')

    parser.add_argument('file', help='path to the ss log file')

    return parser.parse_args()


def get_rampup_times(ss_log_file: str) -> list:
    """Parse an ss log file and return the rampup times of the flows in it.

    Args:
        ss_log_file: the path to an ss log file

    Returns:
        a list of float values that represent the rampup times of all flows,
          in seconds
    """

    rampup_times = []

    with open(ss_log_file) as f:
        lines = f.readlines()

        currently_parsing_flow = False
        logs_of_current_flow = 0
        flow_in_slow_start = True

        for line in lines:
            # One line shows the connection with IP addresses ...
            if EXPERIMENT_IP_RECEIVER in line:
                # If the flow is finishing, reset - if it never left slow
                # start, we can get wrong results if we do not reset
                if line.startswith('FIN'):
                    # If there have been logs and we did not see ssthresh but
                    # do see a FIN, the flow has never left slow start
                    if logs_of_current_flow > 0:
                        # Mark flows that never left slow start with a rampup
                        # time of 0.0
                        rampup_times.append(0.0)
                    currently_parsing_flow = False
                    logs_of_current_flow = 0
                else:  # Otherwise it is active
                    currently_parsing_flow = True
                continue

            # ... and the line after it shows the statistics
            # Hence, consider only the line after the one that shows the
            # connection
            if not currently_parsing_flow:
                continue

            # If flow is still in slow start, ssthresh is not set
            # In the string, the space before ssthresh is important; without
            # it, we also catch rcv_ssthresh, which we don't want
            if not ' ssthresh' in line:
                flow_in_slow_start = True
                logs_of_current_flow += 1
            else:  # Flow has switched to congestion avoidance, so it has set
                   # ssthresh
                # Many logs will contain ssthresh; compute the rampup time
                # only for the first log where this is the case
                if not flow_in_slow_start:
                    currently_parsing_flow = False
                    continue
                rampup_time = \
                        logs_of_current_flow * constants.SS_LOGGING_FREQUENCY
                rampup_times.append(rampup_time)

                logs_of_current_flow = 0
                flow_in_slow_start = False

            # After parsing a statistics line, ignore lines until the next
            # connection line comes up
            currently_parsing_flow = False

    return rampup_times


def main():
    args = parse_args()
    ss_log_file = args.file

    rampup_times = get_rampup_times(ss_log_file)

    for rampup_time in rampup_times:
        print(rampup_time)


if __name__ == '__main__':
    main()

