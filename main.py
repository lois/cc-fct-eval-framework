#!/usr/bin/env python3

"""Framework for congestion control experiments that use FCT as metric.

Prerequisites:
    >= 5 servers: 1 controller, >=2 senders, 1 router, 1 receiver
    The controller must be connected to all other servers (e.g., via a
    switch).
    The senders must be directly connected to the router.
    The router must be directly connected to the receiver.

    Moreover, the servers that are directly connected must be configured
    with the IP addresses defined in the EXPERIMENT_IPS constant.

    The flow completion time measuring tool (fctm) must be available on all
    senders and the receiver. Its executable must be located at
    ~/fct_measuring_tool/fctm
    or if it can be found in another location, you must change the FCTM_PATH
    constant in constants.py to the correct location.

    This script is meant to be run on the controller. The controller
    instructs all other servers via ssh.
"""

import argparse
import sys
import os
import shutil
import time
import random
import logging
from typing import Union
from datetime import datetime
from threading import Thread

import invoke
import fabric

from server_config import *
import constants
import commands
import experiment_util


def parse_args():
    """Parse command line arguments and return args Namespace."""

    parser = argparse.ArgumentParser(description='Run an experiment on your '
            'servers that generates random flows based on datasets and '
            'evaluates the performance using the Flow Completion Time (FCT) '
            'as metric.')
    
    parser.add_argument('config', help='path to an experiment config file')

    verbosity_group = parser.add_mutually_exclusive_group()
    verbosity_group.add_argument('-V', '--verbose', action='store_true',
            help='enable more verbose output (sets logging level to DEBUG)')
    verbosity_group.add_argument('-q', '--quiet', action='store_true',
            help='be less verbose (sets logging level to WARNING)')

    parser.add_argument('-s', '--sender-bottleneck', action='store_true',
            help='sets the sender\'s outgoing interface as bottleneck link '
            'not the router; this option is useful if all links have equal '
            'bandwidth and there is only one sender - in such a case, buffer '
            'backlog and loss happen at the sender, so to obtain data on '
            'these metrics, the measurements are done at the sender not at '
            'the router when using this option')

    parser.add_argument('-c', '--cleanup', action='store_true',
            help='perform only cleanup, i.e. remove configurations from the '
            'servers')

    return parser.parse_args()


def init_ssh_connections(ip_addresses: list = PUBLIC_IPS) -> dict:
    """Initialize ssh connections to all servers.
    
    The controller upholds active ssh connections to every other server
    such that it can instruct them with as little delay as possible.
    This function initializes these connections.

    Don't forget to call close_ssh_connections() when you're done!
    
    Args:
        ip_addresses: a list of IP addresses (as strings) to connect to

    Returns:
        a dict mapping the IP addresses (strings, taken from ip_addresses)
        to their fabric.Connection object
    """

    logging.info('Initializing SSH connections to servers')

    connections = {}
    for ip_address in ip_addresses:
        connection = fabric.Connection(ip_address)
        connections[ip_address] = connection
        logging.debug('Connected to %s', ip_address)

    logging.info('Connected to %d servers', len(ip_addresses))

    return connections


def create_result_directory(
        experiment_name: str,
        results_dir: str = constants.DEFAULT_RESULTS_DIR,
        ) -> str:
    """Create a new directory to store the results of this experiment in.

    This function will create a new directory
      results_dir/experiment_name/yyyymmdd_hhmmss
    to uniquely identify the current experiment.
    It only creates new directories if they do not yet exist.

    Args:
        experiment_name: a string containing the name of the exeriment,
          which will be used for creating a new directory for the current
          experiment in results_dir
        results_dir: a string containing the name of the general results
          directory, i.e., the directory in which all experiment results
          are stored

    Returns:
        the path of the results directory of this experiment
    """

    now = datetime.now()
    time_string = now.strftime('%y%m%d_%H%M%S')
    full_result_directory_path = os.path.join(results_dir, experiment_name,
            time_string)

    os.makedirs(full_result_directory_path, exist_ok=True)

    logging.info('Created result directory: %s', full_result_directory_path)

    return full_result_directory_path


def start_udp_background_traffic_receiver(receiver_connection, duration: int):
    """Start up a listener for UDP background traffic on the receiver.

    Args:
        receiver_connection: a fabric.Connection object that represents the
          connection to the receiver
        duration: the duration in seconds for which the receiver should
          listen for traffic; this should usually be slightly longer than
          the experiment duration
    
    Returns:
        an invoke.runners.Promise object that represents the UDP receiver
          process (don't forget to call join() on this!)
    """

    command = commands.start_udp_receiver(duration)
    udp_receiver = receiver_connection.run(command, asynchronous=True)

    return udp_receiver


def start_udp_background_traffic_sender(
        sender_connection,
        udp_bw: str,
        duration: int,
        ):
    """Start up a UDP background traffic sender on one sender.

    Args:
        sender_connection: a fabric.Connection object that represents the
          connection to the sender
        udp_bw: a string that defines the bandwidth with which the sender
          should send UDP traffic, in the format of iperf (e.g. "15m")
        duration: the duration in seconds for which the sender should send
          traffic; this should usually be the experiment duration

    Returns:
        an invoke.runners.Promise object that represents the UDP sender
          process (don't forget to call join() on this!)
    """

    command = commands.start_udp_sender(EXPERIMENT_IP_RECEIVER, udp_bw,
            duration)
    udp_sender = sender_connection.run(command, asynchronous=True)

    return udp_sender


def compute_bdp(bw: str, rtt: int) -> int:
    """Compute the bandwidth-delay product.

    Args:
        bw: the bandwidth in form of a netem-style string
        rtt: the rtt as integer in milliseconds

    Returns:
        an integer that represents the BDP in packets
    """

    bw_mbit_per_s = experiment_util.bandwidth_str_to_int(bw)
    bw_bit_per_s = bw_mbit_per_s * constants.MBITS_TO_BITS
    bw_byte_per_s = bw_bit_per_s / constants.BITS_PER_BYTE
    packet_size = constants.TCP_PACKET_SIZE + constants.TCP_HEADER_SIZE + \
            constants.IP_HEADER_SIZE
    bw_packets_per_s = bw_byte_per_s / packet_size

    rtt_s = rtt * constants.MILLISECONDS_TO_SECONDS

    return bw_packets_per_s * rtt_s


def configure_sender(
        sender_connection,
        congestion_control_algorithm: str,
        initial_window: int = 10,
        interface_to_router: str = None,
        rate_limit: str = None,
        buffer_size: int = None,
        executable: str = constants.STR_FCTM,
        ):
    """Configure a sender by setting the congestion control algorithm it
    should use and (if desired) setting IW and a rate limit.

    Args:
        sender_connection: a fabric.Connection object that represents the
          connection to the sender
        congestion_control_algorithm: the CCA to use
        initial_window: the initial congestion window (IW) value to use
        interface_to_router: the interface that connects this sender to the
          router; only needed if you add a rate limit
        rate_limit: the sending rate to limit the sender to in the format
          of a bandwidth stirng as used by netem (or None)
        buffer_size: the buffer size in packets to use for the sender link;
          ignored if rate_limit = None
        executable: a string that specifies for this sender which
          executable it should use; can be "fctm" or "pcp". If fctm, set
          CCA using sysctl. If pcp, do not set CCA here
    """
    
    if executable == constants.STR_FCTM:
        cca_command = commands.set_cca(congestion_control_algorithm)
        sender_connection.run(cca_command)

    iw_command = commands.set_initial_window(interface_to_router,
            EXPERIMENT_IP_RECEIVER, EXPERIMENT_IP_ROUTER, initial_window)
    sender_connection.run(iw_command)

    if rate_limit:
        config_str = 'rate {}'.format(rate_limit)
        if buffer_size:
            config_str += ' limit {}'.format(buffer_size)

        rate_command = commands.netem_add(interface_to_router, config_str)
        sender_connection.run(rate_command)


def configure_router_to_receiver(
        router_connection,
        receiver_connection,
        queue_size: int,
        bandwidth: str = None,
        qdisc: str = constants.DEFAULT_QDISC,
        sender_bottleneck: bool = False
        ):
    """Configure the link from the router to the receiver, i.e., the
    bottleneck link, by setting the queue size, bandwidth, and qdisc.

    Args:
        router_connection: a fabric.Connection object that represents the
          connection to the router
        receiver_connection: a fabric.Connection object that represents the
          connection to the receiver; this is needed to set the bandwidth
          (ethtool must be run on both ends of the link)
        queue_size: the queue size (= buffer size) to set, in packets
        bandwidth: the bottleneck bandwidth to set, as a string in the
          format that netem uses
        qdisc: the qdisc to set at the bottleneck, as a string that is
          accepted by tc
        sender_bottleneck: a bool indicating whether the sender's outgoing
          interface is the bottleneck link instead of the router
    """

    # bandwidth
    bw_int = experiment_util.bandwidth_str_to_int(bandwidth)
    if sender_bottleneck:
        interface_btlnk_to_receiver = INTERFACES_SENDERS_TO_ROUTER[0]
        interface_receiver_to_btlnk = INTERFACES_ROUTER_TO_SENDERS[0]
    else:
        interface_btlnk_to_receiver = INTERFACE_ROUTER_TO_RECEIVER
        interface_receiver_to_btlnk = INTERFACE_RECEIVER_TO_ROUTER
    bw_command_router = commands.set_bandwidth(interface_btlnk_to_receiver,
            bw_int)
    router_connection.run(bw_command_router)
    bw_command_receiver = commands.set_bandwidth(interface_receiver_to_btlnk,
            bw_int)
    receiver_connection.run(bw_command_receiver)

    # qdisc
    config_str = qdisc + ' limit {}'.format(queue_size)
    tc_command = commands.qdisc_add(interface_btlnk_to_receiver, config_str)
    router_connection.run(tc_command)


def configure_router_to_senders(
        router_connection,
        rtts: list,
        jitters: list = None,
        loss: float = None,
        slot_mins: list = None,
        slot_maxs: list = None,
        ):
    """Configure the links from the router to all senders by setting the
    RTTs and jitter values.

    Args:
        router_connection: a fabric.Connection object that represents the
          connection to the router
        rtts: a list of integers that contains for each sender its RTT in
          milliseconds
        jitters: a list of integers that contains for each sender its
          jitter in milliseconds
        loss: a list of floats that contains for each sender the random
          packet loss it experiences, as a percentage
        slot_mins: a list of integers that contain for each sender the
          minimum slot time for the returning ACKs in milliseconds
        slot_maxs: a list of integers that contain for each sender the
          maximum slot time for the returning ACKs in milliseconds
    """

    number_senders = len(rtts)
    for i in range(number_senders):
        interface = INTERFACES_ROUTER_TO_SENDERS[i]
        rtt = str(rtts[i]) + 'ms'
        config_str = 'delay {}'.format(rtt)

        if jitters:
            jitter = jitters[i]
            config_str = config_str + ' {}ms'.format(jitter)

        if loss:
            lo = loss[i]
            config_str = config_str + ' loss {}'.format(lo)

        if slot_mins:
            slot_min = slot_mins[i]
            config_str = config_str + ' slot {}ms'.format(slot_min)
        if slot_maxs:
            slot_max = slot_maxs[i]
            config_str = config_str + ' {}ms'.format(slot_max)

        # At 1 Gbit/s, netem must buffer significant numbers of ACKs when
        # emulating delay - here, we increase the limit to a safe value to
        # prevent a reduction in throughput caused by netem dropping ACKs
        limit = 100 * rtts[i]
        config_str += ' limit {}'.format(limit)

        command = commands.netem_add(interface, config_str)
        router_connection.run(command)


def configure_servers(
        connections: list,
        number_senders: int,
        duration: int,
        congestion_control_algorithms: list,
        initial_windows: list,
        queue_size: int,
        bandwidth: str,
        qdisc: str,
        loss: list,
        rtts: list,
        jitters: list,
        slot_mins: list,
        slot_maxs: list,
        udp_background_traffic: list,
        rate_limits: list,
        executables: list,
        sender_bottleneck: bool = False,
        ) -> list:
    """Configure the servers for the experiment.

    This configures the senders and the links from the router to each sender
    as well as from the router to the receiver. This function also starts
    UDP background traffic.

    Args:
        connections: a list of fabric.Connection objects; each one
          represents a connection to one server
        number_senders: the number of senders used in this experiment
        duration: the intended duration of the experiment in seconds
        congestion_control_algorithms: a list of strings that contains for
          each sender the CCA it should use
        initial_windows: a list of integers that contains for each sender
          its IW value
        queue_size: the queue size (= buffer size) to set, in packets
        bandwidth: the bottleneck bandwidth to set, as a string in the
          format that netem uses
        qdisc: the qdisc to set at the bottleneck, as a string that is
          accepted by tc
        loss: a list of floats that contains for each sender the random
          packet loss it experiences, as a percentage
        rtts: a list of integers that contains for each sender its RTT in
          milliseconds
        jitters: a list of integers that contains for each sender its
          jitter in milliseconds
        slot_mins: a list of integers that contain for each sender the
          minimum slot time for the returning ACKs in milliseconds
        slot_maxs: a list of integers that contain for each sender the
          maximum slot time for the returning ACKs in milliseconds
        udp_background_traffic: a list of strings that define for each
          sender how much UDP background traffic it should generate
        rate_limits: a list of strings that define for each sender the
          sending rate it should get limited to
        executables: a list of strings that specify for each sender which
          executable it should use; can be "fctm" or "pcp"
        sender_bottleneck: a bool indicating whether the sender's outgoing
          interface is the bottleneck link instead of the router; this is
          the case when all links have equal bandwidth and there is only
          one sender

    Returns:
        a list of invoke.runners.Promise objects that represent processes
          started as part of this function and must be join()ed at the end
          of the experiment, or None if no such processes exist
    """

    for i in range(number_senders):
        sender_ip = PUBLIC_IPS_SENDERS[i]
        cca = congestion_control_algorithms[i]
        logging.info('Configuring sender %s with CCA %s', sender_ip, cca)

        if initial_windows:
            iw = initial_windows[i]
            logging.info('Configuring IW of sender %s to %d', sender_ip, iw)
        else:
            iw = 10

        if rate_limits and rate_limits[i]:
            rate_limit = rate_limits[i]
            sender_buffer_size = max([1, compute_bdp(rate_limits[i], rtts[i])])
            logging.info('Limiting rate of sender %s to %s', sender_ip,
                    rate_limits[i])
        else:
            rate_limit = None
            sender_buffer_size = None

        executable = executables[i]

        configure_sender(connections[sender_ip], cca, iw,
                INTERFACES_SENDERS_TO_ROUTER[i], rate_limit,
                sender_buffer_size, executable)

    if sender_bottleneck:
        ip_bottleneck = PUBLIC_IPS_SENDERS[0]
        ip_btlnk_receiver = PUBLIC_IP_ROUTER
    else:
        ip_bottleneck = PUBLIC_IP_ROUTER
        ip_btlnk_receiver = PUBLIC_IP_RECEIVER
    logging.info('Configuring link from router %s to receiver %s with queue ' \
            'size %d, bandwidth %s, and qdisc %s',
            ip_bottleneck, ip_btlnk_receiver, queue_size, bandwidth, qdisc)
    configure_router_to_receiver(connections[ip_bottleneck],
            connections[ip_btlnk_receiver], queue_size, bandwidth, qdisc,
            sender_bottleneck)

    logging.info('Configuring links from router %s to senders with RTTs %s ' \
            'and jitters %s and loss %s',
            PUBLIC_IP_ROUTER, str(rtts), str(jitters), str(loss))
    configure_router_to_senders(connections[PUBLIC_IP_ROUTER],
            rtts, jitters, loss, slot_mins, slot_maxs)

    if udp_background_traffic:
        logging.info('Configuring UDP background traffic: %s',
                str(udp_background_traffic))

        udp_processes = []

        udp_receiver_process = start_udp_background_traffic_receiver(
                connections[PUBLIC_IP_RECEIVER], duration + 1)
        udp_processes.append(udp_receiver_process)

        for i in range(number_senders):
            sender_ip = PUBLIC_IPS_SENDERS[i]
            udp_bw = udp_background_traffic[i]
            if udp_bw:
                udp_sender_process = start_udp_background_traffic_sender(
                        connections[sender_ip], udp_bw, duration)
                udp_processes.append(udp_sender_process)

        return udp_processes
    else:
        return None


def measure_buffer_log(
        connection,
        duration: int,
        sender_bottleneck: bool = False,
        ):
    """Begin measuring the buffer backlog at the router.

    Args:
        connection: a fabric.Connection object that represents the
          connection to the router
        duration: an integer that defines for how long the router should
          measure the buffer backlog
        sender_bottleneck: a bool indicating whether the sender's outgoing
          interface is the bottleneck link instead of the router

    Returns:
        an invoke.runners.Promise object that represents the measuring
          process (don't forget to call join() on this!)
    """

    interface = INTERFACES_SENDERS_TO_ROUTER[0] if sender_bottleneck \
            else INTERFACE_ROUTER_TO_RECEIVER
    log_command = commands.start_measuring_buffer_log(interface, duration)
    return connection.run(log_command, asynchronous=True)


def measure_socket_stats(connection, duration: int):
    """Begin measuring socket stats (using ss) at one interface on one
    server.

    Args:
        connection: a fabric.Connection object that represents the
          connection to the server
        duration: an integer that defines for how long the server should
          measure socket stats

    Returns:
        an invoke.runners.Promise object that represents the measuring
          process (don't forget to call join() on this!)
    """

    log_command = commands.start_measuring_socket_stats(duration)
    return connection.run(log_command, asynchronous=True)


def start_measurements(
        connections: list,
        duration: int,
        number_senders: int,
        sender_bottleneck: bool = False,
        ):
    """Begin measurements on the servers.

    The measurements include logging the buffer backlog at the router and
    logging socket stats at the sender.

    Args:
        connections: a list of fabric.Connection objects; each one
          represents a connection to one server
        duration: the duration how long you want to measure, in seconds
        number_senders: the number of senders used in this experiment
        sender_bottleneck: a bool indicating whether the sender's outgoing
          interface is the bottleneck link instead of the router; this is
          the case when all links have equal bandwidth and there is only
          one sender

    Returns:
        an invoke.runners.Promise object for each measuring process (i.e.,
          one for the buffer logging and one for each sender socket stats
          logging) (don't forget to call join()!)
    """

    logging.info('Starting to measure buffer backlog')
    ip_bottleneck = PUBLIC_IPS_SENDERS[0] if sender_bottleneck \
            else PUBLIC_IP_ROUTER
    buffer_process = measure_buffer_log(connections[ip_bottleneck], duration,
            sender_bottleneck)

    logging.info('Starting to measure socket stats on the senders')
    sender_ss_processes = []
    for sender_ip in PUBLIC_IPS_SENDERS[:number_senders]:
        ss_proc = measure_socket_stats(connections[sender_ip], duration)
        sender_ss_processes.append(ss_proc)

    return [buffer_process] + sender_ss_processes


def start_receiver(
        connection,
        duration: int,
        quickack: int,
        executables: list,
        ):
    """Start the receiver / listener process(es).

    Args:
        connection: a fabric.Connection object that represents the
          connection to the receiver
        duration: an integer that defines how long the receiver should
          listen
        quickack: an integer that indicates whether TCP_QUICKACK should
          be enabled (1) or not (0)
        executables: a list of executables (fctm or pcp) that are used and
          for which this function should start a receiver

    Returns:
        a list of invoke.runners.Promise objects that represent the
          receiver processes (don't forget to call join() on this to
          obtain the result!)
    """

    start_fctm_receiver = False
    start_pcp_receiver = False

    if constants.STR_FCTM in executables:
        start_fctm_receiver = True
    if constants.STR_PCP in executables:
        start_pcp_receiver = True

    # Default to always starting an fctm receiver if nothing is specified
    if not start_fctm_receiver and not start_pcp_receiver:
        start_fctm_receiver = True

    receiver_commands = []

    if start_fctm_receiver:
        start_receiver_command = commands.start_receiver_fctm(duration,
                EXPERIMENT_IP_RECEIVER, quickack)
        receiver_commands.append(
                connection.run(start_receiver_command, asynchronous=True))

    if start_pcp_receiver:
        start_receiver_command = commands.start_receiver_pcp(duration)
        receiver_commands.append(
                connection.run(start_receiver_command, asynchronous=True))
        # Hack to prevent a bug where the PCP sender starts up faster than the
        # receiver, causing it to start sending a flow before the receiver is
        # up, which yields strange FCTs
        # A short sleep ensures that the receiver is up when sending commences
        time.sleep(0.1)

    return receiver_commands


def start_sender(
        connection,
        flow_size: int,
        flow_number: int,
        executable: str,
        cca: str,
        ):
    """Start a sender process.

    Args:
        connection: a fabric.Connection object that represents the
          connection to the sender
        flow_size: an integer defining the flow sizes in packets
        flow_number: an integer that uniquely identifies this sender / flow
          within the context of its thread; i.e., sender 1 may send three
          flows: 0, 1, and 2; sender 2 may send two flows: 0, and 1. This
          flow_number is used to generate a unique file to write the flow's
          output into
        executable: a string that defines whether the sender should use
          fctm or a pcp (user level) executable
        cca: if executable is pcp, this specifies the name of the actual
          executable in the pcp-ulevel code to use. Ignored if executable
          is fctm

    Returns:
        an invoke.runners.Promise object that represents the sender process
          (don't forget to call join() on this to obtain the result!!)
    """

    if executable == constants.STR_PCP:
        extra_args = '1' if cca == 'pcp' else ''
        start_sender_command = commands.start_sender_pcp(flow_size, flow_number,
                EXPERIMENT_IP_RECEIVER, cca, extra_args)
    else:  # fctm
        start_sender_command = commands.start_sender_fctm(flow_size, flow_number,
                EXPERIMENT_IP_RECEIVER)

    return connection.run(start_sender_command, asynchronous=True)


def generate_flows(
        connection,
        duration: int,
        flow_sizes: list,
        flow_interarrival_times: list,
        time_scaling_factor: int,
        random_seed: float,
        sender_number: int,
        flow_start_times: list,
        executable: str,
        cca: str,
        ):
    """On one sender, generate flows using flow sizes and interarrival
    times.

    This function is meant to be threaded. Each thread generates flows on
    one sender.

    Args:
        connection: a fabric.Connection object that represents the
          connection to the sender
        duration: an integer that defines how long this sender should
          generate flows in seconds
        flow_sizes: a list containing a flow size dataset to obtain random
          flow sizes from (sizes in packets)
        flow_interarrival_times: a list containing a flow interarrival time
          dataset to obtain random flow interarrival times from (times in
          us)
        time_scaling_factor: an integer for scaling the interarrival times;
          interarrival times should be provided as values in us. For an
          experiment, each value gets multiplied by this factor to achieve a
          sensible value of aggression. For example, a factor of 1000 scales
          the us vales to ms
        random_seed: the seed to use for RNG
        sender_number: the number of this sender (each server that sends
          flows should get assigned an integer to identify it)
        flow_start_times: a list to store the flow start times into
        executable: a string that defines whether the sender should use
          fctm or a pcp (user level) executable
        cca: if executable is pcp, this specifies the name of the actual
          executable in the pcp-ulevel code to use. Ignored if executable
          is fctm
    """

    random.seed(random_seed)

    flow_senders = []
    flow_number = 0

    start_time = time.time()
    while time.time() < start_time + duration:
        flow_size = random.choice(flow_sizes)
        interarrival_time = random.choice(flow_interarrival_times) * \
                time_scaling_factor
        sleep_time = interarrival_time * constants.MICROSECONDS_TO_SECONDS

        logging.info('Sender %d starting flow %d with size %d', sender_number,
                flow_number, flow_size)
        flow_start_time = time.time() - start_time
        s = start_sender(connection, flow_size, flow_number, executable, cca)
        flow_senders.append(s)
        flow_start_times[sender_number].append(flow_start_time)

        flow_number = flow_number + 1

        logging.info('Sender %d sleeping for %d', sender_number, sleep_time)
        time.sleep(sleep_time)

    logging.info('Joining flow sender processes')
    for sender in flow_senders:
        sender.join()
    logging.debug('Done joining flow sender processes')


def force_flow(
        connection,
        start_time: float,
        flow_size: int,
        sender_number: int,
        flow_number: int,
        flow_start_times: list,
        executable: str,
        cca: str,
        ):
    """Generate an enforced flow with a predefined start time and flow size.

    This function is meant to be threaded.
    
    Args:
        connection: a fabric.Connection object that represents the
          connection to the sender
        start_time: a float defining when the flow should start, in seconds
          after the experiment start
        flow_size: an integer defining the flow size in packets
        sender_number: the number of this sender (each server that sends
          flows should get assigned an integer to identify it); only used
          for logging purposes
        flow_number: an integer that uniquely identifies this flow. This
          flow_number is used to generate a unique file to write the flow's
          output into
        flow_start_times: a list to store the flow start times into
        executable: a string that defines whether the sender should use
          fctm or a pcp (user level) executable
        cca: if executable is pcp, this specifies the name of the actual
          executable in the pcp-ulevel code to use. Ignored if executable
          is fctm
    """

    time.sleep(start_time)

    logging.info('Sender %s generating forced flow %d with size %d',
            sender_number, flow_number, flow_size)
    sender = start_sender(connection, flow_size, flow_number, executable, cca)

    flow_start_times[sender_number].append(start_time)

    sender.join()
    logging.debug('Joined forced flow %d', flow_number)


def run_receiver_and_senders(
        connections: list,
        number_senders: int,
        duration: int,
        flow_sizes_per_sender: list,
        flow_interarrival_times_per_sender: list,
        time_scaling_factor: int,
        random_seed: float,
        force_flows: list,
        quickack: int,
        executables: list,
        congestion_control_algorithms: list,
        ) -> list:
    """Let the senders start generating flows based on the experiment
    distributions.

    Args:
        connections: a list of fabric.Connection objects; each one
          represents a connection to one server
        number_senders: the number of senders used in this experiment
        duration: the intended duration of the experiment in seconds
        flow_sizes_per_sender: a list of lists of integers that contains for
          each sender the list of flow sizes to draw from
        flow_interarrival_times_per_sender: a list of lists of integers that
          contains for each sender the list of flow interarrival times to
          draw from
        time_scaling_factor: an integer that is used to scale the flow
          interarrival times (e.g., to scale us to ms)
        random_seed: the seed to use for RNG
        force_flows: a list of lists of (float, int) tuples that defines
          flows to enforce
        quickack: an integer that indicates whether TCP_QUICKACK should
          be enabled (1) or not (0)
        executables: a list of strings that specify for each sender which
          executable it should use; can be "fctm" or "pcp"; if pcp, use
          the CCA specified for the sender as name of the executable in
          the PCP user-level code
        congestion_control_algorithms: a list of strings that specify the
          CCA each sender should use. Ignored if executable is fctm; this
          function only uses this to get the executable name for pcp

    Returns:
        a list of lists that contains for each sender the start times of its
          flows (i.e., at which point in time during the experiment the
          flows were started)
    """

    logging.info('Starting receiver')
    receivers = start_receiver(connections[PUBLIC_IP_RECEIVER], duration,
            quickack, executables)

    sender_threads = []
    flow_start_times = []

    logging.info('Starting sender threads')

    # Random flows
    for i in range(number_senders):
        sender_ip = PUBLIC_IPS_SENDERS[i]
        logging.debug('Starting random sender threads for sender %d (%s)', i,
                sender_ip)

        flow_start_times.append([])

        flow_sizes = flow_sizes_per_sender[i]
        # Ignore "none" dataset that indicates a sender should not generate
        # random flows
        if len(flow_sizes) == 1 and flow_sizes[0] == 0:
            continue
        flow_interarrival_times = flow_interarrival_times_per_sender[i]

        # Each sender should get a different seed (so they don't produce the
        # same flows) in a deterministic way
        seed = random_seed + i * 1000

        executable = executables[i]
        cca = congestion_control_algorithms[i]

        sender_thread = Thread(target=generate_flows,
            args=(connections[sender_ip], max([1, duration - 2]), flow_sizes,
            flow_interarrival_times, time_scaling_factor, seed, i,
            flow_start_times, executable, cca))
        sender_threads.append(sender_thread)
        sender_thread.start()

    # Enforced flows
    for i in range(number_senders):
        sender_ip = PUBLIC_IPS_SENDERS[i]

        if force_flows and force_flows[i] is not None:
            logging.debug('Starting forced flow threads for sender %d (%s)', i,
                    sender_ip)
            flow_number = constants.FORCED_FLOW_INITIAL_FLOW_NUMBER
            for flow in force_flows[i]:
                start_time = flow[0]
                size = flow[1]

                executable = executables[i]
                cca = congestion_control_algorithms[i]

                force_flow_sender_thread = Thread(target=force_flow,
                        args=(connections[sender_ip], start_time, size,
                        i, flow_number, flow_start_times, executable, cca))
                sender_threads.append(force_flow_sender_thread)
                force_flow_sender_thread.start()

                flow_number += 1

    logging.info('Joining sender threads')
    for sender_thread in sender_threads:
        sender_thread.join()
    logging.debug('Done joining sender thread')

    logging.info('Joining receiver thread')
    for receiver in receivers:
        receiver.join()
    logging.debug('Done joining receiver thread')

    return flow_start_times


def join_processes(processes: list):
    """Join processes for which there is an invoke.runners.Promise object
    leftover.

    Args:
        processes: a list of invoke.runners.Promise objects to join
    """

    for process in processes:
        process.join()


def write_flow_start_times_file(
        results_directory: str,
        flow_start_times: list,
        ):
    """Write the flow start times into a file.

    Args:
        results_directory: the path to the result directory of this
          experiment
        flow_start_times: a list of lists that contains for each sender the
          start times of its flows, as returned by run_receiver_and_senders
    """

    with open(os.path.join(results_directory,
            constants.FLOW_START_TIMES_FILE_NAME), 'w') as f:
        logging.info('Writing slow start times into %s', f)
        for i, sender in enumerate(flow_start_times):
            f.write('Sender {}\n'.format(i))
            for flow in sender:
                f.write(str(flow))
                f.write('\n')


def cleanup_configurations(connections: list, number_senders: int):
    """Remove configurations such as netem.

    Args:
        connections: a list of fabric.Connection objects; each one is the
          connection to one server
        number_senders: the number of senders used in this experiment
    """

    logging.info('Cleaning up configurations')

    logging.debug('Removing netem from links from router to senders')
    interfaces_to_senders = INTERFACES_ROUTER_TO_SENDERS[:number_senders]
    for interface in interfaces_to_senders:
        try:
            connections[PUBLIC_IP_ROUTER].run(commands.qdisc_del(interface))
            logging.debug('Successfully removed config from %s on router (%s)',
                    interface, PUBLIC_IP_ROUTER)
        except invoke.exceptions.UnexpectedExit as e:
            logging.info('Failed to remove config from %s on router (%s); '
                    'likely, there was just no config on that interface.',
                    interface, PUBLIC_IP_ROUTER)
            logging.debug('Encountered exception: %s', e)

    logging.debug('Removing netem from links from senders to router')
    ips = PUBLIC_IPS_SENDERS[:number_senders]
    interfaces_to_router = INTERFACES_SENDERS_TO_ROUTER[:number_senders]
    for i, interface in enumerate(interfaces_to_router):
        ip = ips[i]
        try:
            connections[ip].run(commands.qdisc_del(interface))
            logging.debug('Successfully removed config from %s on sender (%s)',
                    interface, ip)
        except invoke.exceptions.UnexpectedExit as e:
            logging.info('Failed to remove config from %s on sender (%s); '
                    'likely, there was just no config on that interface.',
                    interface, ip)
            logging.debug('Encountered exception: %s', e)

    logging.debug('Removing netem from link from router to receiver')
    try:
        connections[PUBLIC_IP_ROUTER].run(
                commands.qdisc_del(INTERFACE_ROUTER_TO_RECEIVER))
    except invoke.exceptions.UnexpectedExit as e:
        logging.info('Failed to remove config from %s on router (%s); '
                'likely, there was just no config on that interface.',
                INTERFACE_ROUTER_TO_RECEIVER, PUBLIC_IP_ROUTER)
        logging.debug('Encountered exception: %s', e)


def retrieve_and_remove_sender_files(
        sender_connections: list,
        results_directory: str,
        ):
    """Retrieve the result files from the senders and store them into the
    local result directory for this experiment. Then, remove them from the
    senders.

    Args:
        sender_connections: a list of fabric.Connection objects; each one is
          the connection to one sender
        results_directory: the path to the result directory of this
          experiment
    """

    logging.info('Retrieving and removing result files from senders')

    for i, sender_connection in enumerate(sender_connections):
        # Get result file names
        ls_command = 'ls {}*'.format(constants.SENDER_RESULT_FILE_PREFIX_FLOW)
        res = None
        try:
            res = sender_connection.run(ls_command, hide=True)
        except invoke.exceptions.UnexpectedExit as e:
            logging.info('Failed to find result files on sender %s; '
                    'likely, that sender just did not produce any flows.',
                    i)
            logging.debug('Encountered exception: %s', e)

        if res:
            filenames = res.stdout.split()
            logging.debug('Files from sender %d: %s', i, str(filenames))
            for filename in filenames:
                # Retrieve
                logging.debug('Retrieving')
                local_result_path = os.path.join(results_directory,
                        '{}{}_{}'.format(
                        constants.SENDER_RESULT_FILE_PREFIX_SENDER, i,
                        filename))
                sender_connection.get(filename, local_result_path)

                # Remove
                logging.debug('Removing result files from sender')
                rm_command = 'rm {}'.format(filename)
                sender_connection.run(rm_command)

        # Get ss log file
        logging.debug('Retrieving ss log from sender')
        local_ss_log_path = os.path.join(results_directory, '{}{}_{}'.format(
                constants.SENDER_RESULT_FILE_PREFIX_SENDER, i,
                constants.SS_LOG_FILE_NAME))
        sender_connection.get(constants.SS_LOG_FILE_NAME, local_ss_log_path)

        # Remove ss log file
        logging.debug('Removing ss log file from sender')
        rm_command = 'rm {}'.format(constants.SS_LOG_FILE_NAME)
        sender_connection.run(rm_command)


def retrieve_and_remove_router_files(
        router_connection,
        results_directory: str,
        ):
    """Retrieve the result files from the router and store them into the
    local result directory for this experiment. Then, remove them from the
    router.

    Args:
        router_connections: a fabric.Connection object that represents the
          connection to the router
        results_directory: the path to the result directory of this
          experiment
    """

    logging.info('Retrieving and removing result files from router')

    # Retrieve
    logging.debug('Retrieving')
    local_result_path = os.path.join(results_directory,
            constants.BUFFER_LOG_FILE_NAME)
    router_connection.get(constants.BUFFER_LOG_FILE_NAME, local_result_path)

    # Remove
    logging.debug('Removing files from router')
    rm_command = 'rm {}'.format(constants.BUFFER_LOG_FILE_NAME)
    router_connection.run(rm_command)


def get_experiment_results(
        connections: list,
        number_senders: int,
        results_directory: str,
        sender_bottleneck: bool = False,
        ):
    """Copy the experiment results from the servers to the controller and
    remove them from the servers.

    Args:
        connections: a list of fabric.Connection objects; each one is the
          connection to one server
        number_senders: the number of senders used in this experiment
        results_directory: the path to the result directory of this
          experiment
        sender_bottleneck: a bool indicating whether the sender's outgoing
          interface is the bottleneck link instead of the router
    """

    sender_connections = [connections[ip] for ip in
            PUBLIC_IPS_SENDERS[:number_senders]]
    router_connection = connections[PUBLIC_IPS_SENDERS[0]] \
            if sender_bottleneck else connections[PUBLIC_IP_ROUTER]

    retrieve_and_remove_sender_files(sender_connections, results_directory)
    retrieve_and_remove_router_files(router_connection, results_directory)


def store_experiment_config(
        config_file: str,
        results_directory: str,
        ):
    """Store the experiment configuration in a file.

    Args:
        config_file: the path to the config file that was provided as input
          for the experiment
        results_directory: the path to the result directory of this
          experiment
    """

    experiment_config_file = os.path.join(results_directory,
            constants.EXPERIMENT_CONFIG_FILE_NAME)
    logging.info('Storing experiment config into %s', experiment_config_file)

    shutil.copy(config_file, experiment_config_file)


def store_random_seed(
        seed: float,
        results_directory: str,
        ):
    """Store the random seed used in the experiment in a file.

    If a new random seed was generated for this experiment (instead of a
    pre-specified one), it can be useful for reproducability to know the
    precise seed value that was generated. Hence, write it into a file.

    Args:
        seed: the random seed used in this experiment
        results_directory: the path to the result directory of this
          experiment
    """

    random_seed_file = os.path.join(results_directory,
            constants.RANDOM_SEED_FILE_NAME)
    logging.info('Storing random seed value into %s', random_seed_file)

    with open(random_seed_file, 'w') as f:
        f.write(str(seed))


def close_ssh_connections(connections: list):
    """Close ssh connections.

    Args:
        connections: a list of fabric.Connection objects; each one is the
          connection to one server
    """

    logging.info('Closing SSH connections')
    for connection in connections:
        connection.close()


def perform_only_cleanup():
    """Only clean up configurations from all servers."""

    ssh_connections = init_ssh_connections(PUBLIC_IPS)
    cleanup_configurations(ssh_connections, len(PUBLIC_IPS_SENDERS))
    close_ssh_connections(list(ssh_connections.values()))


def run_experiment(
        experiment: experiment_util.Experiment,
        config_file: str,
        sender_bottleneck: bool = False,
        results_directory: str = constants.DEFAULT_RESULTS_DIR,
        ):
    """Run an experiment.

    This comprises configuring the servers according to the parameteres set
    in the experiment object, generating flows, measuring values, and
    storing the results in a newly created directory on the controller.

    Args:
        experiment: an Experiment object that defines all parameters for
          running this experiment.
        config_file: a str representing the path to the experiment's config
          file
        sender_bottleneck: a bool indicating whether the sender's outgoing
          interface is the bottleneck link instead of the router; this is
          the case when all links have equal bandwidth and there is only
          one sender
        results_directory: the directory in which to store the experiment
          result in
    """

    number_senders = experiment.number_senders
    experiment_ips = PUBLIC_IPS_SENDERS[:number_senders] + \
            [PUBLIC_IP_ROUTER, PUBLIC_IP_RECEIVER]

    ssh_connections = init_ssh_connections(experiment_ips)

    experiment_result_dir = \
            create_result_directory(experiment.name, results_directory)

    quickack_int = 1 if experiment.quickack else 0
    executables = experiment.executables if experiment.executables else \
            [constants.STR_FCTM] * number_senders

    config_processes = configure_servers(ssh_connections, number_senders,
            experiment.duration, experiment.congestion_control_algorithms,
            experiment.initial_windows,
            experiment.queue_size, experiment.bandwidth, experiment.qdisc,
            experiment.loss, experiment.rtts, experiment.jitters,
            experiment.slot_mins, experiment.slot_maxs,
            experiment.udp_background_traffic, experiment.rate_limits,
            executables, sender_bottleneck)
    measurement_processes = \
            start_measurements(ssh_connections, experiment.duration,
            number_senders, sender_bottleneck)

    flow_start_times = run_receiver_and_senders(ssh_connections, number_senders,
            experiment.duration,
            experiment.flow_sizes_per_sender,
            experiment.flow_interarrival_times_per_sender,
            experiment.time_scaling_factor,
            experiment.random_seed,
            experiment.force_flows,
            quickack_int,
            executables, experiment.congestion_control_algorithms
    )

    processes = [p for p in measurement_processes]
    if config_processes:
        processes.extend(config_processes)
    join_processes(processes)

    cleanup_configurations(ssh_connections, number_senders)

    write_flow_start_times_file(experiment_result_dir, flow_start_times)
    get_experiment_results(ssh_connections, number_senders,
            experiment_result_dir, sender_bottleneck)
    store_experiment_config(config_file, experiment_result_dir)
    store_random_seed(experiment.random_seed, experiment_result_dir)

    close_ssh_connections(list(ssh_connections.values()))


def main():
    args = parse_args()
    config_file = args.config

    if args.verbose:
        log_level = logging.DEBUG
    elif args.quiet:
        log_level = logging.WARNING
    else:
        log_level = logging.INFO

    logging.basicConfig(level=log_level, format='%(levelname)s: %(message)s')

    if args.cleanup:
        perform_only_cleanup()
        sys.exit()

    sender_bottleneck = True if args.sender_bottleneck else False

    experiment = experiment_util.from_config(config_file)

    if experiment.number_senders > len(PUBLIC_IPS_SENDERS):
        logging.error('number_senders provided in the config file is larger ' \
                'than the number of sender IP addresses. Please reduce ' \
                'number_senders or provide more sender IP addresses.')
        sys.exit(1)

    run_experiment(experiment, config_file, sender_bottleneck)


if __name__ == '__main__':
    main()

