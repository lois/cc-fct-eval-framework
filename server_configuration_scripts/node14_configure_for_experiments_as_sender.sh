#!/bin/sh

MY_IP='10.10.2.16'

EXPERIMENT_PREFIX_SENDER='13.37.1.0/24'
EXPERIMENT_IP_SENDER1='13.37.1.0'

EXPERIMENT_IP_ROUTER='13.37.2.0'
EXPERIMENT_IP_RECEIVER='13.37.3.0'

DEV_TO_ROUTER='eth0'


sudo ip addr add ${EXPERIMENT_IP_SENDER1}/24 dev $DEV_TO_ROUTER
sudo ip route del $EXPERIMENT_PREFIX_SENDER dev $DEV_TO_ROUTER

sudo ip route add $EXPERIMENT_IP_ROUTER dev $DEV_TO_ROUTER
sudo ip route add $EXPERIMENT_IP_RECEIVER via $EXPERIMENT_IP_ROUTER dev $DEV_TO_ROUTER


sudo sysctl -w net.ipv4.tcp_allowed_congestion_control="cubic reno bbr bbr2"

sudo sysctl -w net.core.rmem_max="50000000"
sudo sysctl -w net.core.wmem_max="100000000"
sudo sysctl -w net.ipv4.tcp_rmem="4096 87380 50000000"
sudo sysctl -w net.ipv4.tcp_wmem="4096 65536 100000000"

sudo ethtool -K $DEV_TO_ROUTER tso off gso off gro off

