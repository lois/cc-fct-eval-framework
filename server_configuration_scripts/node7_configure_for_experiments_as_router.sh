#!/bin/sh

MY_IP='10.10.2.12'

EXPERIMENT_IP_SENDER1='13.37.1.0'
EXPERIMENT_IP_SENDER2='13.37.1.1'
EXPERIMENT_IP_SENDER3='13.37.1.2'
EXPERIMENT_IP_SENDER4='13.37.1.3'

EXPERIMENT_PREFIX_ROUTER='13.37.2.0/24'
EXPERIMENT_IP_ROUTER='13.37.2.0'
EXPERIMENT_IP_RECEIVER='13.37.3.0'

DEV_TO_SENDER1='eth0'
DEV_TO_SENDER2='eth1'
DEV_TO_SENDER3='eth3'
DEV_TO_SENDER4='eth5'

DEV_TO_RECEIVER='eth2'


sudo ip addr add ${EXPERIMENT_IP_ROUTER}/24 dev $DEV_TO_SENDER1
sudo ip route del $EXPERIMENT_PREFIX_ROUTER dev $DEV_TO_SENDER1
sudo ip addr add ${EXPERIMENT_IP_ROUTER}/24 dev $DEV_TO_SENDER2
sudo ip route del $EXPERIMENT_PREFIX_ROUTER dev $DEV_TO_SENDER2
sudo ip addr add ${EXPERIMENT_IP_ROUTER}/24 dev $DEV_TO_SENDER3
sudo ip route del $EXPERIMENT_PREFIX_ROUTER dev $DEV_TO_SENDER3
sudo ip addr add ${EXPERIMENT_IP_ROUTER}/24 dev $DEV_TO_SENDER4
sudo ip route del $EXPERIMENT_PREFIX_ROUTER dev $DEV_TO_SENDER4
sudo ip addr add ${EXPERIMENT_IP_ROUTER}/24 dev $DEV_TO_RECEIVER
sudo ip route del $EXPERIMENT_PREFIX_ROUTER dev $DEV_TO_RECEIVER

sudo ip route add $EXPERIMENT_IP_SENDER1 dev $DEV_TO_SENDER1
sudo ip route add $EXPERIMENT_IP_SENDER2 dev $DEV_TO_SENDER2
sudo ip route add $EXPERIMENT_IP_SENDER3 dev $DEV_TO_SENDER3
sudo ip route add $EXPERIMENT_IP_SENDER4 dev $DEV_TO_SENDER4
sudo ip route add $EXPERIMENT_IP_RECEIVER dev $DEV_TO_RECEIVER

sudo sysctl -w net.ipv4.ip_forward=1
sudo iptables -t filter -P FORWARD ACCEPT


sudo sysctl -w net.core.rmem_max="50000000"
sudo sysctl -w net.core.wmem_max="100000000"
sudo sysctl -w net.ipv4.tcp_rmem="4096 87380 50000000"
sudo sysctl -w net.ipv4.tcp_wmem="4096 65536 100000000"

sudo ethtool -K $DEV_TO_RECEIVER tso off gso off gro off
sudo ethtool -K $DEV_TO_SENDER1 tso off gso off gro off
sudo ethtool -K $DEV_TO_SENDER2 tso off gso off gro off
sudo ethtool -K $DEV_TO_SENDER3 tso off gso off gro off
sudo ethtool -K $DEV_TO_SENDER4 tso off gso off gro off

