# Congestion Control Slowdown Evaluation Framework
This is a framework for conducting congestion control experiments with flows
that are _generated using datasets_ where the main metric is _slowdown_
(normalized Flow Completion Time (FCT)).
This repository contains files for running experiments (see `main.py`) as well
as for summarizing the results of an experiment (see `analyze.py`).

The majority of researchers conduct congestion control experiments by sending
unlimitedly backlogged flows through a link (e.g., using iperf) and evaluating
the achieved _throughput_.
It is then considered an ideal result when all coexisting flows obtain the same
share of the bandwidth (this is called flow rate fairness).
However, flows in the Internet have a certain size and not an unlimited data
backlog; indeed, flows are often rather short.
Hence, a setup where flows of certain sizes (not infinite flows) are generated
is significantly more realistic.
Moreover, users tend to care about their flows finishing fast.
Therefore, the primary metric this framework uses is slowdown, i.e., the FCT
normalized by the theoretically optimal FCT.
Slowdown is better than raw FCT because it is a relative value, making it
possible to compare the slowdown of differently sized flows and making the
results intuitive.

For more information on the problems with the traditional evaluation setup for
congestion control, on slowdown as a metric, and on initial experiments
conducted with this framework, please refer to our paper: \
Adrian Zapletal and Fernando Kuipers. _Slowdown as a Metric for Congestion Control Fairness_. ACM HotNets 2023.


## Setup

This framework is made to run on physical servers.
Therefore, to use it, you must set up multiple servers that are connected to
each other (ideally via direct links):
* one controller (ideally a separate server, but one of the other servers can
also co-function as the controller)
* multiple senders
* one router (a server that functions as a router between senders and receiver)
* one receiver

All senders and the receiver must be connected to the router (ideally via direct
links).
The controller must be connected to all servers; this should be a separate
connection (can be via a switch).

After physically connecting the servers, you must configure them such that the
senders can send data to the receiver.
To do this, assign new IP addresses to all servers and add IP routes for those
IP addresses via the interfaces through which the servers are connected.
The recommended IP addresses are: `13.37.1.n` for senders with a different
value for `n` for each sender; `13.37.2.0` for the router; and `13.37.3.0` for
the receiver.
Moreover, make sure the controller can reach all servers, e.g., via a switch.
The IP addresses through which the controller can reach the other servers are
refered to as _public IPs_.
The IP addresses through which the servers send data to each other are refered
to as _experiment IPs_.
While the controller is intended to be a separate machine, it can also be the
same physical server as one of the others (that server's public IP is then
127.0.0.1).

You must enable IP forwarding on the router.
Further recommended configurations on the servers are: increase Linux TCP buffer
sizes on all servers; turn off offloading mechanisms such as TSO, GSO, and GRO
on all servers; enable the congestion control algorithms you want to use on the
senders.
You can refer to the scripts in
`server_configuration_scripts/` to see how all of this can be done (these are the
scripts that we used to configure our servers for our experiments).

You must enable passwordless sudo on all servers.
You can do so by adding `user ALL=(ALL) NOPASSWD: ALL` to the `/etc/sudoers`
file on all servers, where `user` should be replaced by your username.

After setting up and configuring the servers,
install this framework on the controller by running the following command:
```
$ git clone --recurse-submodules https://gitlab.tudelft.nl/lois/cc-fct-eval-framework.git
```
The framework requires Python 3 and the Python module fabric.
Install fabric by running `pip3 install fabric`.
The plotting functionality of this framework requires numpy and matplotlib.
Install them by running `pip3 install numpy matplotlib`.

You need to provide the correct public IPs and experiment IPs as well as the
interfaces through which the servers are inter-connected.
Edit `server_config.py` and write
* the correct IP addresses into the constants
`PUBLIC_IPS_SENDERS`, `PUBLIC_IP_ROUTER`, `PUBLIC_IP_RECEIVER`,
`EXPERIMENT_IPS_SENDERS`, `EXPERIMENTS_IP_ROUTER`, and `EXPERIMENT_IP_RECEIVER`,
* the correct names of the interfaces into the constants
`INTERFACES_ROUTER_TO_SENDERS` and `INTERFACE_ROUTER_TO_RECEIVER`.

Install the Flow Completion Time Measuring (FCTM) tool on all senders and the
receiver.
This is the tool that generates the flows and measures their FCT.
The tool is available here: <https://gitlab.tudelft.nl/lois/fctm> or as a
submodule inside this repository.
The README in that repository contains instructions on how to compile fctm.
You must compile it at the location defined by `FCTM_PATH` in `constants.py`,
which by default is `fctm/fctm` (a binary called `fctm` inside a directory
called `fctm` that is located in the user's home directory).

Optional:
If you want to use user-level sender implementations built on top of the PCP
codebase, install them on all senders and the receiver by compiling them at
the location defined by `PCP_BASE_PATH` in `constants.py`.
Our version of this codebase, which includes implementations of PCP, Jump
Start, and Halfback, is available here:
<https://gitlab.tudelft.nl/lois/pcp-ulevel> or as a submodule inside this
repository.

On the router and on the senders, copy the loop perl script to the location
defined by `LOOPPL_PATH` in `constants.py`, which by default is `loop.pl` (a
perl script located in the user's home directory).
This script is needed for tracking the buffer backlog and for logging socket
statistics at regular intervals.


## Usage

To run an experiment, you need a _config_ file for setting parameters and
_datasets_ for generating flows.
You need to provide datasets for flow sizes and flow inter-arrival times.
There are example datasets in the directory `datasets/` and we provide real
datasets extracted from a MAWI trace in `mawi200610-datasets.tar.gz`.
Refer to the information in the Flow Analysis repository
(<https://gitlab.tudelft.nl/lois/flow-analysis>) for more information on how to
generate datasets.
For more information on config files, refer to `configs/template`, an example
config file with explanations about parameters and how to correctly provide
values.

Then, to run an experiment, use this command on the controller:
```
$ ./main.py configs/<your_config>
```
This creates a new directory in `results/` where the results of your experiment
are stored.

To obtain nice summaries and plots, use:
```
$ ./analyze.py results/<path_to_your_experiment_results>
```

### Example
To illustrate the framework usage, we provide an example where we want to
evaluate different congestion control algorithms in a scenario where one long
flow (which we enforce as a predefined flow) coexists with random short flows
(which we generate using datasets).
The example assumes that all servers have been set up as explained above.

The first step is to generate one or more config files for the experiment(s).
A config file `example_config` in our example could look like this:
```
# A config for experiments with two senders;
# the first sender sends a long forced flow and
# the second sender generates random flows from a dataset.
duration:                           30
number_senders:                     2
bandwidth:                          100mbit
queue_size:                         83
rtts:                               20, 20
congestion_control_algorithms:      cubic, cubic
# sender 1 uses no datasets, sender 2 uses the specified datasets for flow generation
flow_sizes_per_sender:              datasets/none, datasets/flow_sizes/size_dataset
flow_interarrival_times_per_sender: datasets/none, datasets/flow_interarrival_times/interarrival_time_dataset
random_seed:                        3.14159
# sender 1 starts a forced flow after 1s, sender 2 uses no forced flows
force_flows:                        1 230000, 0
```
Please refer to `configs/template` for more information on these and other
possible parameters.

The datasets could be derived from actual traces (e.g., using the tools from
<https://gitlab.tudelft.nl/lois/flow-analysis>).
They contain a list of all possible values,
from which the evaluation framework draws random values.
This way, one can generate randomized flows from a distribution that corresponds
exactly to the distribution of flows in a trace.

We run an experiment for our `example_config` config file using
```
$ ./main.py configs/example_config
```
This automatically sets the parameters at the servers, generates the flows as
desired, and stores results in a results directory in `results/`.
The `results/` directory contains one directory for each config file; these
directories are named by the config files.
Within those config file directories, there is one directory for each repetition
of the experiment; these directories are named by the time and date the
experiment was run.
In our example, we obtain the results directory
`results/example_config/240618_143000/`.
This results directory contains the experiment config, the random seed, and the
flow start times (i.e., the experiment setup), as well as for each sender a FCT
result file.
A FCT result file is named like `sender0_fct_eval_results_flow_1000`;
this name identifies the sender (the server that sent the flow) and the flow
itself.
Each of those files contains the flow's size and FCT.

To generate summaries and plots for this results directory, we must run
```
$ ./analyze.py results/example_config/240618_143000/
```
This automatically generates various files in the results directory:
a metrics summary that lists mean FCT, mean slowdown, maximum slowdown, and
those values in an idealized network that uses Shortest Remaining
Processing Time (SRPT) as an optimal result to compare to;
a slowdown summary that lists detailed information for each flow (its FCT,
size, theoretically optimal FCT, and slowdown);
and a plot that visualizes all flows over time.

After this, we can repeat the experiment with different parameter values.
To automatically generate many config files for many combinations of parameters,
one can use `config_generator.py`.
To create sophisticated plots that summarize multiple experiments (i.e., a
variety of parameter values like in our HotNets '23 paper), there are the
scripts `summarize_results.py`, `summarize_slowdown_per_flow_size_results.py`,
and `summarize_slowdown_per_flow_results.py`.


## Repository Content

This repository contains the following directories and files.

### Main Executables
For one experiment:
* `main.py`: The executable that runs an experiment. See Usage section in this
README for more information.
* `analyze.py`: The executable that analyzes the results of an experiment and
generates textual summaries and plots. See Usage section in this README for more
information.

To summarize results of many experiments:
* `summarize_results.py`: Functions for plotting the mean and maximum slowdown
across many parameters; an example is Figure 5 in our HotNets '23 paper.
This type of result is the primary intended use case of this framework.
It is useful for a novel type of fairness experiments where one observes whether
flows experience unnecessary slowdown (unfairness) with different algorithms and
parameter settings.
The script likely requires some changes to generate precisely the plot you
desire.
* `summarize_slowdown_per_flow_size_results.py`: Functions for plotting the
slowdown per flow size in an experiment; an example is Figure 4 in our
HotNets '23 paper.
This type of result is useful for analyzing the performance of flows of
different sizes.
The script likely requires some changes to generate precisely the plot you
desire.
* `summarize_slowdown_per_flow_results.py`: Functions for plotting the slowdown
per flow in an experiment; an example is Figure 6 in our HotNets '23 paper.
The script likely requires some changes to generate precisely the plot you
desire.

### Informational Files
* `server_configuration_scripts/` directory: Contains shell scripts that we
used to configure our servers for experiments.
These scripts can be used to see how the server configuration should be done and
you can use them as a basis for making similar scripts for your servers.
* `experiment_naming_guide.txt`: Explains recommendations on systematic file
names to use for config files.
* `mawi200610-datasets.tar.gz`: Datasets of flow sizes and flow inter-arrival
times from a MAWI trace, as an example for real datasets.
They are contained inside a .tar.gz not inside the `datasets/` directory (where
they belong) in order to save a few MB of space.

### Code Related to Running Experiments
* `server_config.py`: Defines variables that are required for experiments and
depend on the setup of servers, e.g., IP addresses and interface names.
* `configs/` directory: The directory to store your config files for experiment
parameters in.
Contains a template config file that explains all possible parameters and how to
use them in the config.
* `datasets/` directory: The directory to store your datasets for generating
flows (with random sizes and inter-arrival times) in.
Contains two sub-directories: `flow_sizes/` for flow size datasets and
`flow_interarrival_times/` for flow inter-arrival time datasets.
Each sub-directory contains one example dataset.
The `datasets/` directory also contains the empty dataset `none`, which you can
use for senders that do not generate randomized flows but only forced flows in
an experiment.
* `fctm` submodule: A git submodule that links to the tool used for sending and
receiving flows.
* `pcp-ulevel` submodule: A git submodule that links to our version of the PCP
user level codebase (with a tool for receiving flows and several
implementations for sending flows).
* `config_generator.py`: A tool for automatically generating multiple config
files with many combinations of parameters.
* `experiment_util.py`: Functions and classes related to experiment configs.
Specifically, this file contains the class used for storing an experiment config
and a function for generating an instance of this class from a config file.
* `commands.py`: Functions for creating commands that the controller sends to
the other servers (e.g., for server configuration or for starting processes).
* `constants.py`: Constants used throughout the whole framework.
* `loop.pl`: A script that calls a command at certain intervals with high
precision.
This is used for tracking the buffer backlog at the router and for logging
socket statistics at the sender.

### Code Related to Analyzing Results & Generating Plots
* `compute_metrics.py`: Functions for computing metrics such as mean FCT, mean
slowdown, maximum slowdown, and the optimal values with SRPT.
Can be executed to print these metrics for a given results directory.
* `compute_slowdown.py`: Functions for computing a flow's slowdown given its
FCT.
Can be executed to print the slowdown of one flow in a results directory (by
using the flow's FCT file in the results).
* `srpt.py`: Functions for simulating an idealized network that runs SRPT
scheduling at the bottleneck.
This is used to obtain ideal values for slowdown to compare experiment results
to.
Can be executed to run SRPT for a simple example (for testing purposes).
* `summarize_flows.py`: Functions for creating a slowdown summary, which stores
for all flows their start time, FCT, size, optimal FCT, and slowdown, as well as
for generating a neat plot that visualizes all flows in an experiment over time.
Can be executed to write the slowdown summary into a file and generate the flow
summary plot.
* `plot_buffer_log.py`: Functions for plotting the buffer backlog at
the bottleneck router over time.
* `get_rampup_times.py`: Functions for extracting the rampup times of flow in
an experiment, i.e., the time after which a flow transitions from slow start
into congestion avoidance.

