#!/usr/bin/env python3

"""A tool for generating many config files with many different parameter
configurations.
"""

import argparse
import itertools
import os


def parse_args():
    """Parse command line arguments."""

    parser = argparse.ArgumentParser(
            description='Generate config files with all combinations of '
                    'the parameters you specify.\n'
                    'You should specify one for each dataset, seed, forced '
                    'flows, and time scaling factor; then, specify as many of '
                    'the other parameters (RTTs, CCAs, etc.) as you want as a '
                    'a semicolon-separated list. This tool will automatically '
                    'generate all possible combinations of these parameters.\n'
                    'Don\'t forget to enclose semicolon-separated values or '
                    'values that contain spaces in "" so that your shell '
                    'understands it.\n'
                    'For help regarding the meaning of parameters, consult \n'
                    'configs/template.\n'
                    '\n'
                    'Example usage:\n'
                    'config_generator.py test_experiment -o configs/ '
                    '-d 30 -n 3 -b 100mbit -q "10;100;250;750" '
                    '-r "20,20,20;40,40,40;60,60,60;20,40,60" '
                    '-c "cubic,cubic,cubic;bbr,bbr,bbr" '
                    '-s datasets/my_flow_sizes -i datasets/my_arrival_times '
                    '-t 1000 -r 3.14159 -f "2 50000, 0, 0"\n')

    parser.add_argument('name', help='the experiment name; this will be used '
            'as the base name for all config files, i.e., as the "traffic '
            'description"')
    parser.add_argument('-o', '--output-directory', help='the directory to '
            'store the config files in. This should usually be configs/')

    parser.add_argument('-d', '--duration')
    parser.add_argument('-n', '--number-senders')
    parser.add_argument('-b', '--bandwidth')
    parser.add_argument('-Q', '--qdisc')
    parser.add_argument('-q', '--queue-size')
    parser.add_argument('-l', '--loss')
    parser.add_argument('-r', '--rtts')
    parser.add_argument('-j', '--jitters')
    parser.add_argument('-c', '--congestion-control-algorithms')
    parser.add_argument('-w', '--initial-windows')
    parser.add_argument('-s', '--flow-sizes-per-sender')
    parser.add_argument('-i', '--flow-interarrival-times-per-sender')
    parser.add_argument('-t', '--time-scaling-factor')
    parser.add_argument('-R', '--random-seed')
    parser.add_argument('-f', '--force-flows')
    parser.add_argument('-u', '--udp-background-traffic')
    parser.add_argument('-a', '--rate-limit')
    parser.add_argument('-A', '--quickack')
    parser.add_argument('-x', '--executables')
    parser.add_argument('-S', '--slot-mins')
    parser.add_argument('-Z', '--slot-maxs')

    args = parser.parse_args()
    return args


def parse_input_string(input_str: str) -> list:
    """Parse an string that contains a semicolon-separated list of values
    and return the values as a list of strings."""

    return [value.strip() for value in input_str.split(';')]


def main():
    args = parse_args()

    name = args.name
    output_directory = args.output_directory if args.output_directory else ''

    # Generate lists of parameters
    duration = parse_input_string(args.duration) if args.duration else []
    number_senders = parse_input_string(args.number_senders) \
            if args.number_senders else []
    bandwidth = parse_input_string(args.bandwidth) if args.bandwidth else []
    qdisc = parse_input_string(args.qdisc) if args.qdisc else []
    queue_size = parse_input_string(args.queue_size) if args.queue_size else []
    loss = parse_input_string(args.loss) if args.loss else []
    rtts = parse_input_string(args.rtts) if args.rtts else []
    jitters = parse_input_string(args.jitters) if args.jitters else []
    congestion_control_algorithms = \
            parse_input_string(args.congestion_control_algorithms) \
            if args.congestion_control_algorithms else []
    initial_windows = parse_input_string(args.initial_windows) \
            if args.initial_windows else []
    flow_sizes_per_sender = parse_input_string(args.flow_sizes_per_sender) \
            if args.flow_sizes_per_sender else []
    flow_interarrival_times_per_sender = \
            parse_input_string(args.flow_interarrival_times_per_sender) \
            if args.flow_interarrival_times_per_sender else []
    time_scaling_factor = parse_input_string(args.time_scaling_factor) \
            if args.time_scaling_factor else []
    random_seed = parse_input_string(args.random_seed) \
            if args.random_seed else []
    force_flows = parse_input_string(args.force_flows) \
            if args.force_flows else []
    udp_background_traffic  = parse_input_string(args.udp_background_traffic) \
            if args.udp_background_traffic else []
    rate_limit = parse_input_string(args.rate_limit) if args.rate_limit else []
    quickack = parse_input_string(args.quickack) if args.quickack else []
    executables = parse_input_string(args.executables) \
            if args.executables else []
    slot_mins = parse_input_string(args.slot_mins) if args.slot_mins else []
    slot_maxs = parse_input_string(args.slot_maxs) if args.slot_maxs else []

    # Create all combinations of parameters
    all_lists = [number_senders, duration, bandwidth, qdisc, queue_size,
            congestion_control_algorithms, initial_windows, rtts, jitters, loss,
            udp_background_traffic, rate_limit,
            flow_sizes_per_sender, flow_interarrival_times_per_sender,
            time_scaling_factor, random_seed, force_flows,
            quickack, executables, slot_mins, slot_maxs]
    all_lists = [l for l in all_lists if len(l) > 0]
    all_combinations = list(itertools.product(*all_lists))

    # Generate the config files
    for config in all_combinations:
        config_str = ''  # the string to write into the file
        config_file_name = ''

        i = 0

        if args.number_senders:
            n = config[i]
            config_str += 'number_senders:                     ' + n
            config_str += '\n'
            config_file_name += n
            config_file_name += '-'
            i += 1

        config_file_name += name
        config_file_name += '-'

        if args.duration:
            d = config[i]
            config_str += 'duration:                           ' + d
            config_str += '\n'
            config_file_name += d + 's'
            config_file_name += '-'
            i += 1

        if args.bandwidth:
            b = config[i]
            config_str += 'bandwidth:                          ' + b
            config_str += '\n'
            config_file_name += b
            config_file_name += '-'
            i += 1

        if args.qdisc:
            qd = config[i]
            config_str += 'qdisc:                              ' + qd
            config_str += '\n'
            config_file_name += qd.replace(' ', '_')
            config_file_name += '-'
            i += 1

        if args.queue_size:
            q = config[i]
            config_str += 'queue_size:                         ' + q
            config_str += '\n'
            config_file_name += q + 'p'
            config_file_name += '-'
            i += 1

        if args.congestion_control_algorithms:
            c = config[i]
            config_str += 'congestion_control_algorithms:      ' + c
            config_str += '\n'
            config_file_name += c
            config_file_name += '-'
            i += 1

        if args.initial_windows:
            w = config[i]
            config_str += 'initial_windows:                    ' + w
            config_str += '\n'
            config_file_name += 'iws' + w
            config_file_name += '-'
            i += 1

        if args.rtts:
            r = config[i]
            config_str += 'rtts:                               ' + r
            config_str += '\n'
            config_file_name += r
            config_file_name += '-'
            i += 1

        if args.jitters:
            j = config[i]
            config_str += 'jitters:                            ' + j
            config_str += '\n'
            config_file_name += j
            config_file_name += '-'
            i += 1

        if args.loss:
            l = config[i]
            config_str += 'loss:                               ' + l
            config_str += '\n'
            config_file_name += l
            config_file_name += '-'
            i += 1

        if args.udp_background_traffic:
            u = config[i]
            config_str += 'udp_background_traffic:             ' + u
            config_str += '\n'
            config_file_name += 'udp' + u
            config_file_name += '-'
            i += 1

        if args.rate_limit:
            a = config[i]
            config_str += 'rate_limit:                         ' + a
            config_str += '\n'
            config_file_name += 'ratelimit' + a
            config_file_name += '-'
            i += 1

        if args.flow_sizes_per_sender:
            s = config[i]
            config_str += 'flow_sizes_per_sender:              ' + s
            config_str += '\n'
            i += 1

        if args.flow_interarrival_times_per_sender:
            it = config[i]
            config_str += 'flow_interarrival_times_per_sender: ' + it
            config_str += '\n'
            i += 1

        if args.time_scaling_factor:
            t = config[i]
            config_str += 'time_scaling_factor:                ' + t
            config_str += '\n'
            i += 1

        if args.random_seed:
            rs = config[i]
            config_str += 'random_seed:                        ' + rs
            config_str += '\n'
            i += 1

        if args.force_flows:
            f = config[i]
            config_str += 'force_flows:                        ' + f
            config_str += '\n'
            i += 1

        if args.quickack:
            qa = config[i]
            config_str += 'quickack:                           ' + qa
            config_str += '\n'
            config_file_name += 'qa' + qa
            config_file_name += '-'
            i += 1

        if args.executables:
            x = config[i]
            config_str += 'executables:                        ' + x
            config_str += '\n'
            i += 1

        if args.slot_mins:
            sm = config[i]
            config_str += 'slot_mins:                          ' + sm
            config_str += '\n'
            config_file_name += 'slotmins' + sm
            config_file_name += '-'
            i += 1

        if args.slot_maxs:
            zm = config[i]
            config_str += 'slot_maxs:                          ' + zm
            config_str += '\n'
            config_file_name += 'slotmaxs' + zm
            config_file_name += '-'
            i += 1

        # Remove final "-" from file name
        config_file_name = config_file_name[:-1]

        # Write config file
        output_path = os.path.join(output_directory, config_file_name)
        with open(output_path, 'w') as config_file:
            config_file.write(config_str)


if __name__ == '__main__':
    main()

