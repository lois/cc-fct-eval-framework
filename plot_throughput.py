#!/usr/bin/env python3

"""Functions for plotting the throughput of flows over time.
"""

import argparse
import sys
import os
import matplotlib.pyplot as plt

import experiment_util
import constants
import summarize_flows
from server_config import *


def parse_args():
    """Parse command line arguments.

    Returns:
        the args Namespace from argparse
    """

    parser = argparse.ArgumentParser(description='Plot the throughput of ' + \
            'flows over time.')

    parser.add_argument('path', help='the path to the results directory; ' + \
            'note that you must provide one or more sender IDs using the ' + \
            '-f and/or -p options')
    parser.add_argument('-f', '--fctm', nargs='+', help='use this option ' + \
            'for senders that used fctm to send flows to provide one or ' + \
            'more IDs of senders whose throughput you want to analyze')
    parser.add_argument('-p', '--pcp', nargs='+', help='use this option ' + \
            'for senders that used a pcp-ulevel sending script to provide ' + \
            'one or more IDs of the senders whose throughput you want ' + \
            'to analyze')
    parser.add_argument('-o', '--output', help='path to a file to store ' + \
            'plot into; if omitted, show plot directly')
    parser.add_argument('-d', '--duration', action='store_true',
            help='fix the length of the plot\'s x-axis to the experiment ' + \
            'duration')
    parser.add_argument('-t', '--time', nargs=2, help='plot only a ' + \
            'certain time period; you must provide two values here: the ' + \
            'start and the end of the period you want to plot. The ' + \
            'values should be floats that represent the moments in time ' + \
            'in seconds')
    parser.add_argument('-s', '--flows-per-sender', nargs='+', help='to ' + \
            'color flows of each sender in a similar color, provide the ' + \
            'number of flows of each sender using this option')
    parser.add_argument('-r', '--rtt-factor', help='a factor to multiply ' + \
            'the RTT with in order to make the plot more clean / readable')

    return parser.parse_args()


def get_duration(experiment_config_file: str) -> int:
    """Obtain the experiment duration from an experiment config file.

    Args:
        experiment_config_file: the path to an experiment config file that
          lists all parameters of an experiment

    Returns:
        an int representing the experiment duration in seconds
    """

    experiment = experiment_util.from_config(experiment_config_file)
    return experiment.duration


def get_rtts(experiment_config_file: str) -> list:
    """Obtain the RTT values from an experiment config file.

    Args:
        experiment_config_file: the path to an experiment config file that
          lists all parameters of an experiment

    Returns:
        a list of ints where each element represents the RTT of one sender
          in ms
    """

    experiment = experiment_util.from_config(experiment_config_file)
    return experiment.rtts


def parse_ss_log_file(
        ss_log_file: str,
        rtt: int,
        rtt_factor: int = 1,
        ) -> list:
    """Parse an ss log file and return the throughput values of the flows
    in it.

    Use this function to obtain the throughput of all flows when the sender
    used fctm for sending.

    Args:
        ss_log_file: the path to an ss log file
        rtt: the RTT in ms used by the sender
        rtt_factor: a factor to multiply the RTT with. This can be useful
          when there are flows that fill the buffer and/or the RTT is low,
          causing the measurements to fluctaute erratically. A factor of,
          e.g., 2, can be useful to make the plot more readable

    Returns:
        a list of lists of float values where each list of float values
          represents the throughput of one flow, in Mbps
    """

    throughput_values = [[]]


    with open(ss_log_file) as f:
        lines = f.readlines()

        log_index = -1
        flows = []
        current_flow = None

        prev_bytes_sent = {}  # one entry per flow that keeps the previous
                              # value of bytes_sent such that we can use the
                              # delta to calculate the sending rate

        # ss logging interval in ms
        logging_interval = constants.SS_LOGGING_FREQUENCY * 1000
        rtt = rtt * rtt_factor
        # Normally, we try to get the sending rate every rtt
        # If RTT is not divisible by logging_interval, we get the sending rate
        # first every <rtt rounded down to multiple of logging interval> and
        # then every <rtt rounded up to multiple of logging interval>
        # (e.g., for rtt=25ms and logging_interval=10ms: rate intervals are
        # 20ms (every 2 measurements) and 30ms (every 3 measurements); for
        # rtt=40ms and logging_interval=10ms: both intervals are 40ms (every 4
        # measurements). If RTT < logging_interval, just get the throughput
        # for every measurement
        if rtt < logging_interval:
            rate_interval1 = 1
            rate_interval2 = 1
        elif rtt % logging_interval == 0:
            rate_interval1 = rtt / logging_interval
            rate_interval2 = rtt / logging_interval
        else:
            rate_interval1 = int(rtt / logging_interval)  # round down
            rate_interval2 = int(rtt / logging_interval) + 1  # round up

        count1 = {}  # one entry per flow that counts up to rate_interval1
        count2 = {}  # one entry per flow that counts up to rate_interval2

        current_count = {}  # tracks for each flow whether we are currently in
                            # count1 or in count2
        current_interval_duration = 0  # duration of the current rate interval
                                       # in ms, for calculating sending rate
        interval_duration1 = rate_interval1 * logging_interval
        interval_duration2 = rate_interval2 * logging_interval

        # Every log gets an entry in throughput_values
        # If no experiment flow was active, put None
        # Else put the flow's throughput
        for line in lines:
            split_line = line.split()
            if line.startswith('State'):  # First line of ss -i output
                # New entry in throughput_values
                log_index += 1
                for flow in range(len(throughput_values)):
                    throughput_values[flow].append(None)  # Default to None

            # One line shows the connection with IP addresses ...
            if EXPERIMENT_IP_RECEIVER in line:
                current_flow = split_line[3]
                if current_flow not in flows:  # New flow
                    flows.append(current_flow)
                    if len(flows) > 1:
                        throughput_values.append([None] * (log_index + 1))
                    count1[current_flow] = 1
                    count2[current_flow] = 1
                    current_count[current_flow] = 1
                    prev_bytes_sent[current_flow] = 0
                continue

            # ... and the line after it shows the statistics
            # Hence, consider only the line after the one that shows the
            # connection
            if not current_flow:
                continue

            # We calculate the sending rate as bytes sent per RTT
            # The "send" attribute of ss uses cwnd/rtt to calculate the rate,
            # but this could be incorrect, e.g., if the sender is rwnd-
            # limited; similarly, ss shows the pacing_rate, but that could be
            # incorrect when rwnd-limited, too. Thus, the best estimate we can
            # get is the actual bytes sent per RTT.
            if 'bytes_sent' in line:
                # Update counts; if the current count hits the rate interval,
                # we use the bytes_sent value to get a measurement
                if current_count[current_flow] == 1:
                    if count1[current_flow] < rate_interval1:
                        count1[current_flow] += 1
                        # not using this measurement
                        # copy previous throughput_value so that within one
                        # rate interval, the throughput stays the same
                        throughput_values[
                                flows.index(current_flow)][log_index] = \
                                0 if log_index == 0 \
                                else throughput_values[
                                        flows.index(current_flow)][log_index-1]
                        current_flow = None
                        continue
                    else:  # count1 has reached rate_interval1
                        count1[current_flow] = 1
                        current_count[current_flow] = 2
                        current_interval_duration = interval_duration1
                        # using this measurement
                else:
                    if count2[current_flow] < rate_interval2:
                        count2[current_flow] += 1
                        # not using this measurement
                        throughput_values[
                                flows.index(current_flow)][log_index] = \
                                throughput_values[
                                        flows.index(current_flow)][log_index-1]
                        current_flow = None
                        continue
                    else:  # count2 has reached rate_interval2
                        count2[current_flow] = 1
                        current_count[current_flow] = 1
                        current_interval_duration = interval_duration2
                        # using this measurement

                # The line contains something like "bytes_sent:42"
                # Extract the number (42 in the example) as cur_bytes_sent
                bytes_sent_index = [i for i, elem in enumerate(split_line) \
                        if elem.startswith('bytes_sent')][0]
                cur_bytes_sent = \
                        int(split_line[bytes_sent_index].split(':')[1])
                bytes_sent_delta = \
                        cur_bytes_sent - prev_bytes_sent[current_flow]

                # Calculate sending rate
                sending_rate_bytes_per_ms = \
                        bytes_sent_delta / current_interval_duration
                sending_rate_bit_per_ms = \
                        sending_rate_bytes_per_ms * constants.BITS_PER_BYTE
                # Convert to Mbps by default
                sending_rate = sending_rate_bit_per_ms / 1000

                prev_bytes_sent[current_flow] = cur_bytes_sent
            else:
                # If the bytes_sent attribute is not in the statistics, put a
                # sending rate of 0. This usually happens when the SYN has
                # been sent but there is not yet a reply
                sending_rate = 0

            throughput_values[flows.index(current_flow)][log_index] = \
                    sending_rate

            # After parsing a statistics line, ignore lines until the next
            # connection line comes up
            current_flow = None

    return throughput_values


def parse_fct_eval_results_file(fct_eval_results_file: str) -> list:
    """Parse an fct eval results file of one flow and return the throughput
    values for that flow.

    Use this function to obtain the throughput of one flow when the sender
    used a pcp-ulevel sending script.

    Args:
        fct_eval_results_file: the path to an fct eval results file

    Returns:
        a list of float values that represent the throughput of this flow,
          in Mbps
    """

    throughput_values = []

    with open(fct_eval_results_file) as f:
        lines = f.readlines()

    for line in lines:
        # The send attribute tells us the current sending rate
        if 'send' in line:
            # The sending rate is listed as, e.g., "send 95422160bps"
            # somewhere on the line
            split_line = line.split()
            send_index = split_line.index('send')
            sending_rate_index = send_index + 1
            sending_rate_str = split_line[sending_rate_index]
            # the unit is always bps
            sending_rate_bps_str = sending_rate_str.split('b')[0]
            if not sending_rate_bps_str.isdigit():
                continue
            sending_rate_bps = int(sending_rate_bps_str)
            # Convert to Mbps by default
            sending_rate = sending_rate_bps / constants.MBITS_TO_BITS

            throughput_values.append(sending_rate)

    return throughput_values


def parse_all_flows_of_pcp_sender(
        results_directory: str,
        sender: str,
        ) -> list:
    """Get and parse all fct eval results files of one sender and return
    the throughput values of all flows.

    Use this function to obtain the throughput of all flows when the sender
    used a pcp-ulevel sending script.

    Args:
        results_directory: the path to the results directory that contains
          all relevant files
        sender: the ID of the sender for which the throughput should be
          analyzed

    Returns:
        a list of lists of float values where each list of float values
          represents the throughput of one flow, in Mbps
    """

    throughput_values = []

    all_fct_files = summarize_flows.find_fct_files(results_directory)
    fct_files_for_sender = sorted([f for f in all_fct_files if f.startswith(
            constants.SENDER_RESULT_FILE_PREFIX_SENDER + sender)
            and 'fct' in f])

    flow_start_times_file = os.path.join(results_directory,
            constants.FLOW_START_TIMES_FILE_NAME)
    all_flow_start_times = summarize_flows.parse_flow_start_times_file(
            flow_start_times_file)
    flow_start_times_sender = all_flow_start_times[int(sender)]

    # NOTE we currently assume that the PCP logging frequency is equal to the
    # logging frequency (hence, we use the ss logging frequency constant here)!
    logging_frequency = constants.SS_LOGGING_FREQUENCY

    i = 0
    for flow_start_time, flow_fct_file in zip(flow_start_times_sender,
            fct_files_for_sender):
        throughput_values.append([])
        if flow_start_time > 0:  # fill gaps
            # How many logs do we need to fill in?
            gap_size = int(flow_start_time / logging_frequency)
            # Use None before a flow starts, which is gap_size times before
            # the current flow starts
            for j in range(gap_size):
                throughput_values[i].append(None)

        flow_throughput_values = parse_fct_eval_results_file(
                os.path.join(results_directory, flow_fct_file))
        throughput_values[i].extend(flow_throughput_values)

        i += 1

    return throughput_values


def plot_throughput(
        all_throughput_values: list,
        output_file: str = None,
        experiment_duration: int = None,
        time_start: float = None,
        time_end: float = None,
        flows_per_sender: list = [],
        ):
    """Use matplotlib to plot the throughput of flows over time.

    Args:
        all_throughput_values: a list of lists of float values; each list
          of float values represents the throughput of flows on a sender
        output_file: a path to a file to store the plot into; if None,
          display the plot directly
        experiment_duration: the duration of the experiment; used to cap
          the x-axis at this value. If time_start and time_end are
          provided, they overwrite this cap
        time_start: if provided together with time_end, plot only from
          time_start to time_end
        time_end: if provided together with time_start, plot only from
          time_start to time_end
        flows_per_sender: a list of ints that tells this function for
          every sender how many flows that sender has; this is useful for
          coloring the flows of each sender in a similar color
    """

    fig, ax = plt.subplots()

    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Sending rate [mbit/s]')

    if experiment_duration:
        ax.set_xlim(0, experiment_duration)

    if time_start and time_end:
        ax.set_xlim(time_start, time_end)

    # colors for when flows_per_sender is used
    colors_per_sender = [
            ['#006', '#009', '#00c', '#00f'],
            ['#060', '#090', '#0c0', '#0f0'],
            ['#600', '#900', '#c00', '#f00'],
            ['#606', '#909', '#c0c', '#f0f'],
            ]

    ax.grid(axis='y', visible=True)

    sender = 0  # for when flows_per_sender is used
    flow = 0    # for when flows_per_sender is used
    for throughput_values in all_throughput_values:
        y = throughput_values
        x = [i * constants.SS_LOGGING_FREQUENCY for i in range(len(y))]

        if flows_per_sender:  # color flows of a sender similarly
            sid = sender % len(colors_per_sender)
            sender_colors = colors_per_sender[sid]
            fid = flow % len(sender_colors)
            ax.plot(x, y, color=colors_per_sender[sid][fid])

            if flow < flows_per_sender[sender] - 1:
                flow += 1
            else:
                sender += 1
                flow = 0
        else:  # just use automatic colors
            ax.plot(x, y)

    plt.tight_layout()

    if output_file:
        plt.savefig(output_file)
    else:
        plt.show()


def main():
    args = parse_args()
    path = args.path
    fctm_ids = args.fctm if args.fctm else []
    pcp_ids = args.pcp if args.pcp else []
    if not (fctm_ids or pcp_ids):
        print('You must provide sender IDs using -f and/or -p!')
        sys.exit(1)

    output_file = args.output if args.output else None
    time_start = float(args.time[0]) if args.time else None
    time_end = float(args.time[1]) if args.time else None
    flows_per_sender = [int(s) for s in args.flows_per_sender] \
            if args.flows_per_sender else []
    rtt_factor = int(args.rtt_factor) if args.rtt_factor else 1

    experiment_config_file = \
            os.path.join(path, constants.EXPERIMENT_CONFIG_FILE_NAME)
    experiment_duration = get_duration(experiment_config_file) \
            if args.duration else None
    rtts = get_rtts(experiment_config_file)

    all_throughput_values = []

    for fctm_id in fctm_ids:
        ss_log_file_name = constants.SENDER_RESULT_FILE_PREFIX_SENDER + \
                fctm_id + '_' + constants.SS_LOG_FILE_NAME
        ss_log_file_path = os.path.join(path, ss_log_file_name)
        throughput_values = \
                parse_ss_log_file(ss_log_file_path, rtts[int(fctm_id)],
                                  rtt_factor)
        all_throughput_values.extend(throughput_values)

    for pcp_id in pcp_ids:
        throughput_values = parse_all_flows_of_pcp_sender(path, pcp_id)
        all_throughput_values.extend(throughput_values)

    plot_throughput(all_throughput_values, output_file, experiment_duration,
                    time_start, time_end, flows_per_sender)


if __name__ == '__main__':
    main()

