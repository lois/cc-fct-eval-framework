#!/usr/bin/env python3

"""Functionality for simulating Shortest Remaining Processing Time first
(SRPT) scheduling."""

import constants
import experiment_util


def _get_processing_time_per_packet(bandwidth: str) -> int:
    """Calculate how long it takes to process one packet at the given
    bandwidth, assuming a default packet size.

    Args:
        bandwidth: the bandwidth as a string like "10mbit"

    Returns:
        the time in microseconds it takes to process a packet at the given
          bandwidth
    """

    bw_mbit_per_s = experiment_util.bandwidth_str_to_int(bandwidth)
    bw_bit_per_s = bw_mbit_per_s * constants.MBITS_TO_BITS
    packet_size_byte = constants.TCP_PACKET_SIZE + \
                constants.TCP_HEADER_SIZE + constants.IP_HEADER_SIZE
    packet_size_bit = packet_size_byte * constants.BITS_PER_BYTE

    seconds_per_packet = packet_size_bit / bw_bit_per_s
    us_per_packet = seconds_per_packet / constants.MICROSECONDS_TO_SECONDS

    return int(us_per_packet)


def _compute_srpt(
        flow_start_times: list,
        flow_sizes: list,
        time_unit: int = 1,
        rate_limited_times: list = [],
        ) -> list:
    """Compute a Shortest Remaining Processing Time (SRPT) schedule and
    return the FCTs obtained with it.

    This function only computes SRPT for flows (or jobs). It does not take
    into account bandwidth in mbit/s, packets, etc. There are just flows
    that have a size of s units and start at a time t and the scheduler can
    process exactly one size unit per time unit.

    Args:
        flow_start_times: a list (of size n) that contains the times at
          which the flows start
        flow_sizes: a list (of size n) that contains the flow sizes; when
          using this function to simulate SRPT for real network conditions,
          the flow sizes should be in microseconds (i.e.,
          flow_size_in_packets * time_unit)
        time_unit: the amount of time it takes to process one size unit;
          when using this function to simulate SRPT for real network
          conditions, this should be the time in microseconds it takes to
          process one packet at the bottleneck bandwidth
        rate_limited_times: a list (of size n) that contains for each flow
          the time units it takes to process a size unit with it; this is
          to allow for rate-limited flows where data arrives slower; if a
          flow is not rate-limited, its entry in this list should be None

    Returns:
        a list (of size n) that contains for each flow its flow completion
          time (FCT) in seconds as obtained using the SRPT scheduler
    """

    n = len(flow_sizes)
    flow_completion_times = [0] * n

    remaining_processing_times = [size for size in flow_sizes]
    queued_up = [0] * n  # allow queueing up units for rate-limited flows
    current_time = min(flow_start_times)

    # While flows remain that need to be processed
    while sum(remaining_processing_times) > 0:
        # Get flows that could be processed currently
        available_flows = []
        for i in range(n):
            if flow_start_times[i] <= current_time \
                    and remaining_processing_times[i] > 0:
                # Flow is not rate-limited
                if not rate_limited_times or not rate_limited_times[i]:
                    available_flows.append(i)
                else:  # Flow is rate-limited
                    # New unit arrives at the limited rate
                    if (current_time + time_unit) % rate_limited_times[i] < \
                            time_unit:
                        available_flows.append(i)
                        queued_up[i] += 1
                    else:  # No new packet arrives ...
                        if queued_up[i] > 0:  # ... but units are queued up
                            available_flows.append(i)

        current_time += time_unit

        if not available_flows:
            continue

        # Find shortest flow among the available ones
        shortest_flow = -1
        shortest_size = float('inf')
        for i in available_flows:
            remain = remaining_processing_times[i]
            if remain < shortest_size:
                shortest_size = remain
                shortest_flow = i

        # Process shortest flow
        remaining_processing_times[shortest_flow] -= time_unit

        # If shortest flow is rate-limited, dequeue one unit
        if rate_limited_times and rate_limited_times[shortest_flow]:
            queued_up[shortest_flow] -= 1

        # Compute FCT if flow has ended
        if remaining_processing_times[shortest_flow] <= 0:
            flow_completion_times[shortest_flow] = \
                    current_time - flow_start_times[shortest_flow]

    return flow_completion_times


def compute_srpt_for_experiment(
        flow_start_times: list,
        flow_sizes: list,
        bandwidth: str,
        rtts: list,
        rate_limits: list,
        ) -> tuple:
    """Compute a Shortest Remaining Processing Time (SRPT) schedule and
    return the FCTs and slowdowns obtained with it.

    This function computes SRPT for flows as seen in an experiment. Flow
    start times should be in seconds and flow sizes should be in packets.
    This function simulates SRPT for the flows at the given bandwidth and
    computes the FCTs. It also adds the RTT to that FCT time. It returns
    the total FCT including 1 RTT.

    Args:
        flow_start_times: a list (of size n) that contains the times at
          which the flows start, in seconds after the experiment start
        flow_sizes: a list (of size n) that contains the flow sizes in
          packets
        bandwidth: the bottleneck bandwidth as a string like "10mbit"
        rtts: a list (of size n) that contains for every flow its RTT in
          milliseconds
        rate_limits: a list (of size n) that contains for every flow the
          rate it is limited to (or None if the flow is not rate-limited)

    Returns:
        a tuple containing two lists: one list (of size n) that contains
          the flow completion times (FCTs) as obtained using the SRPT
          scheduler and one list (of size n) that contains the
          corresponding slowdowns
    """

    us_per_packet = _get_processing_time_per_packet(bandwidth)
    flow_sizes_in_us = [size * us_per_packet for size in flow_sizes]
    flow_start_times_in_us = [int(time / constants.MICROSECONDS_TO_SECONDS)
            for time in flow_start_times]
    rtts_in_s = [rtt * constants.MILLISECONDS_TO_SECONDS for rtt in rtts]
    rtts_in_us = [rtt / constants.MICROSECONDS_TO_SECONDS for rtt in rtts_in_s]

    # if rate_limits is None, convert to empty list to prevent errors
    if not rate_limits:
        rate_limits = []

    rate_limited_us_per_packet = []
    for rate_limit in rate_limits:
        current_us_per_packet = _get_processing_time_per_packet(rate_limit) \
                if rate_limit is not None else None
        rate_limited_us_per_packet.append(current_us_per_packet)

    scheduling_fcts_in_us = _compute_srpt(flow_start_times_in_us,
            flow_sizes_in_us, us_per_packet, rate_limited_us_per_packet)
    scheduling_fcts_in_s = [fct * constants.MICROSECONDS_TO_SECONDS
            for fct in scheduling_fcts_in_us]

    final_fcts = [scheduling_fcts_in_s[i] + rtts_in_s[i]
            for i in range(len(scheduling_fcts_in_s))]

    # Calculate slowdowns
    slowdowns = []
    for i, fct in enumerate(scheduling_fcts_in_us):
        # Due to the way the SRPT function represents flow sizes in units of
        # time, a flow's theoretical optimal FCT is simply its size plus RTT
        # (except for rate limited flows, for which we need to add a factor)
        rtt = rtts_in_us[i]
        final_fct = fct + rtt
        # Rate limited flow? Get factor
        if rate_limited_us_per_packet and rate_limited_us_per_packet[i]:
            flow_size_factor = rate_limited_us_per_packet[i] / us_per_packet
        else:
            flow_size_factor = 1

        optimal_fct = flow_sizes_in_us[i] * flow_size_factor + rtt

        slowdown = final_fct / optimal_fct
        slowdowns.append(slowdown)

    return final_fcts, slowdowns


def main():
    # Just for testing purposes
    start_times = [0, 0.0012, 0.0048, 0.0072, 0.012, 0.0192, 0.0204]  # in s
    flow_sizes = [2, 8, 2, 3, 1, 2, 2]  # in packets
    bw = '10mbit'
    rtts = [20] * len(flow_sizes)  # in ms
    #rtts = [0] * len(flow_sizes)  # in ms
    rate_limits = [None] * len(flow_sizes)
    rate_limits[0] = '1mbit'

    fcts, slowdowns = compute_srpt_for_experiment(start_times, flow_sizes,
            bw, rtts, rate_limits)
    print('FCTs:', fcts)
    print('Slowdowns:', slowdowns)


if __name__ == '__main__':
    main()

