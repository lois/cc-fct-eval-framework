#!/usr/bin/env python3

"""Functions for computing the slowdown of one flow given the results in a
FCT result file.
"""

import argparse
import os

import experiment_util
import constants


def parse_args():
    """Parse command line arguments.
    
    Returns:
        the args Namespace from argparse
    """
    parser = argparse.ArgumentParser(description='Parse a flow FCT result ' + \
            'file (to obtain the FCT and flow size) and an experiment ' + \
            'config file (to obtain the bandwidth) and compute the flow\'s ' + \
            'slowdown.')

    parser.add_argument('fctfile', help='path to the buffer log file')
    parser.add_argument('experimentconfig', help='path to the experiment ' + \
            'config file (for reading the bandwidth)')
    parser.add_argument('-o', '--output', help='path to a file to store ' + \
            'the result into; if omitted, just print to stdout')

    return parser.parse_args()


def parse_fct_file(fct_file: str) -> tuple:
    """Parse an FCT result file and obtain the FCT and flow size.

    Args:
        fct_file: a path to a FCT result file

    Returns:
        a tuple containing a float (for the FCT in seconds) and an int (for
          the flow size in packets)
    """
    with open(fct_file) as f:
        lines = f.readlines()

        fct = None
        flow_size = None

        for line in lines:
            split = line.split(':')
            if len(split) < 2:
                continue
            value = split[1].strip()

            if line.startswith('FCT'):  # e.g., "FCT: 1.337s"
                fct = float(value[:-1])
            elif line.startswith('Size'):  # e.g., "Size: 128"
                flow_size = int(value)

        return fct, flow_size


def get_sender_index_from_fct_file_name(fct_file: str) -> int:
    file_name = os.path.basename(fct_file)
    if not 'fct' in file_name:  # could be ss log
        return None

    split = file_name.split('_')
    sender_str = split[0]  # returns something like "sender0", "sender12"
    return int(sender_str[6:])


def get_flow_index_from_fct_file_name(fct_file: str) -> int:
    file_name = os.path.basename(fct_file)
    split = file_name.split('_')
    flow_index = split[-1]  # returns something like "0", "4"
    return int(flow_index)


def get_values_for_slowdown_computation(
        experiment_config_file: str,
        sender_index: int,
        ) -> tuple:
    """Obtain the bottleneck bandwidth, the RTT and flow rate limit of one
    sender and the amount of UDP background traffic from an experiment
    config file.

    Args:
        experiment_config_file: the path to an experiment config file that
          lists all parameters of an experiment
        sender_index: the index of the sender to obtain the RTT from, e.g.,
          1 to obtain the RTT of the second sender

    Returns:
        a tuple containing four int values; one for the bandwidth, one for
          the RTT, one for the UDP traffic, and one for the rate limit
    """

    experiment = experiment_util.from_config(experiment_config_file)

    bw = experiment_util.bandwidth_str_to_int(experiment.bandwidth)

    rtt = experiment.rtts[sender_index]

    if experiment.udp_background_traffic is None:
        udp = 0
    else:
        udp_values = []
        for val in experiment.udp_background_traffic:
            if val:
                udp_values.append(experiment_util.bandwidth_str_to_int(val))
            else:
                udp_values.append(0)
        udp = sum(udp_values)

    if experiment.rate_limits is None:
        rate_limit = float('inf')
    else:
        rate_limit = experiment_util.bandwidth_str_to_int(
                experiment.rate_limits[sender_index])
        rate_limit = float('inf') if rate_limit is None else rate_limit
        
    return bw, rtt, udp, rate_limit


def compute_slowdown(
        fct: float,
        flow_size: int,
        bandwidth: int,
        rtt: int,
        udp: int,
        rate_limit: int,
        ) -> tuple:
    """Compute the flow's slowdown using raw values as input.
    
    Args:
        fct: the flow's flow completion time in seconds
        flow_size: the flow size in packets
        bandwidth: the bottleneck bandwidth in mbit/s
        rtt: the flow's RTT in ms
        udp: the total amount of UDP background traffic in mbit/s (0 if no
          UDP background traffic)
        rate_limit: the flow's rate limit in mbit/s (float('inf') if no
          rate limit)

    Returns:
        a tuple of two floats: the flow's slowdown and the optimal FCT
    """

    # Make sure we use the same units for everything: seconds and bits
    fct_s = fct

    bytes_per_packet = constants.TCP_PACKET_SIZE + constants.TCP_HEADER_SIZE + \
            constants.IP_HEADER_SIZE + constants.ETHERNET_HEADER_SIZE
    bits_per_packet = bytes_per_packet * constants.BITS_PER_BYTE
    flow_size_bit = flow_size * bits_per_packet

    bandwidth_bit_per_s = bandwidth * constants.MBITS_TO_BITS
    rate_limit_bit_per_s = rate_limit * constants.MBITS_TO_BITS
    udp_bit_per_s = udp * constants.MBITS_TO_BITS
    # Optimal FCT might be limited by UDP background traffic or a rate limit
    max_bw_bit_per_s = min([rate_limit_bit_per_s,
        bandwidth_bit_per_s - udp_bit_per_s])

    rtt_s = rtt * constants.MILLISECONDS_TO_SECONDS

    # Compute (theoretical) optimal FCT in seconds
    optimal_s = (flow_size_bit / max_bw_bit_per_s) + rtt_s

    # Compute slowdown
    slowdown = fct_s / optimal_s

    return slowdown, optimal_s


def compute_slowdown_from_files(
        fct_file: str,
        experiment_config_file: str,
        ) -> tuple:
    """Compute the flow's slowdown using the FCT result file (to read the
    FCT and flow size from) and the experiment config file (to read the
    bandwidth and RTT from) as input.

    Args:
        fct_file: a path to a FCT result file
        experiment_config_file: a path to an experiment config file

    Returns:
        a tuple of two floats: the flow's slowdown and the optimal FCT
    """

    fct, flow_size = parse_fct_file(fct_file)
    sender_index = get_sender_index_from_fct_file_name(fct_file)
    bw, rtt, udp, rate_limit = get_values_for_slowdown_computation(
            experiment_config_file, sender_index)

    return compute_slowdown(fct, flow_size, bw, rtt, udp, rate_limit)


def main():
    args = parse_args()
    fct_file = args.fctfile
    experiment_config_file = args.experimentconfig
    output_file = args.output if args.output else None

    slowdown, _ = compute_slowdown_from_files(fct_file, experiment_config_file)

    if output_file:
        with open(output_file, 'w') as f:
            f.write(slowdown)
    else:
        print(slowdown)


if __name__ == '__main__':
    main()

