#!/usr/bin/env python3

"""Functions for computing metrics for an experiment result.
These metrics include mean FCT, mean and maximum slowdown for the
experiment as well as the same metrics for SRPT as the optimum.
"""

import argparse
import os

import constants
import summarize_flows
import experiment_util
import srpt


def parse_args():
    """Parse command line arguments."""

    parser = argparse.ArgumentParser(description='Compute metrics for an '
            'experiment result such as mean FCT, proximity of mean FCT to the '
            'theoretical optimum, mean slowdown, and max slowdown.')

    parser.add_argument('directory', help='the path to the directory that '
            'contains the experiment result files')

    return parser.parse_args()


def build_result_summary_string(
        mean_fct,
        mean_slowdown,
        max_slowdown,
        srpt_mean_fct,
        srpt_mean_slowdown,
        srpt_max_slowdown,
        ):
    result_summary = ''

    result_summary += 'Mean FCT: ' +  str(mean_fct) + '\n'
    result_summary += 'Mean Slowdown: ' + str(mean_slowdown) + '\n'
    result_summary += 'Max Slowdown: ' + str(max_slowdown) + '\n'
    result_summary += 'Mean FCT / SRPT Mean FCT: ' + str(mean_fct /
            srpt_mean_fct) + '\n'
    result_summary += '\n'
    result_summary += 'SRPT Mean FCT: ' + str(srpt_mean_fct) + '\n'
    result_summary += 'SRPT Mean Slowdown: ' + str(srpt_mean_slowdown) + '\n'
    result_summary += 'SRPT Max Slowdown: ' + str(srpt_max_slowdown) + '\n'

    return result_summary


def generate_metrics_summary(results_directory: str) -> str:
    fct_files = summarize_flows.find_fct_files(results_directory)
    flow_start_times_file = os.path.join(results_directory,
            constants.FLOW_START_TIMES_FILE_NAME)
    experiment_config_file = os.path.join(results_directory,
            constants.EXPERIMENT_CONFIG_FILE_NAME)

    slowdown_summary = summarize_flows.create_slowdown_summary(fct_files,
            flow_start_times_file, experiment_config_file, results_directory)

    experiment = experiment_util.from_config(experiment_config_file)
    sender_rtts = experiment.rtts
    sender_rate_limits = experiment.rate_limits
    bandwidth = experiment.bandwidth

    flow_start_times = []
    fcts = []
    flow_sizes = []
    slowdowns = []
    flow_rtts = []
    flow_rate_limits = []

    for i, sender in enumerate(slowdown_summary):
        for flow in sender:
            start_time = flow[0]
            flow_start_times.append(start_time)
            fct = flow[1]
            fcts.append(fct)
            flow_size = flow[2]
            flow_sizes.append(flow_size)
            slowdown = flow[4]
            slowdowns.append(slowdown)
            rtt = sender_rtts[i]
            flow_rtts.append(rtt)
            rate_limit = sender_rate_limits[i] if sender_rate_limits else None
            flow_rate_limits.append(rate_limit)

    mean_fct = sum(fcts) / len(fcts)
    mean_slowdown = sum(slowdowns) / len(slowdowns)
    max_slowdown = max(slowdowns)

    srpt_fcts, srpt_slowdowns = srpt.compute_srpt_for_experiment(
            flow_start_times, flow_sizes, bandwidth, flow_rtts,
            flow_rate_limits)

    srpt_mean_fct = sum(srpt_fcts) / len(srpt_fcts)
    srpt_mean_slowdown = sum(srpt_slowdowns) / len(srpt_slowdowns)
    srpt_max_slowdown = max(srpt_slowdowns)

    return build_result_summary_string(mean_fct, mean_slowdown, max_slowdown,
            srpt_mean_fct, srpt_mean_slowdown, srpt_max_slowdown)


def write_metrics_to_file(
        results_directory: str,
        metrics_file_name: str = constants.METRICS_SUMMARY_FILE_NAME,
        ):
    result_summary = generate_metrics_summary(results_directory)
    metrics_file = os.path.join(results_directory, metrics_file_name)
    with open(metrics_file, 'w') as f:
        f.write(result_summary)


def main():
    args = parse_args()
    results_directory = args.directory

    result_summary = generate_metrics_summary(results_directory)
    print(result_summary)


if __name__ == '__main__':
    main()

