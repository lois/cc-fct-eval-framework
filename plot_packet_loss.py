#!/usr/bin/env python3

"""Functions for plotting the packet loss over time.
"""

import argparse
import matplotlib.pyplot as plt

import experiment_util
import constants


def parse_args():
    """Parse command line arguments.

    Returns:
        the args Namespace from argparse
    """

    parser = argparse.ArgumentParser(description='Parse a buffer log file ' + \
            'and create a plot showing the packet loss over time.')

    parser.add_argument('file', help='path to the buffer log file')
    parser.add_argument('-o', '--output', help='path to a file to store ' + \
            'plot into; if omitted, show plot directly')
    parser.add_argument('-e', '--experimentconfig', help='path to the ' + \
            'experiment config file (useful for reading the qdisc)')

    return parser.parse_args()


def get_qdisc(experiment_config_file: str) -> str:
    """Obtain the qdisc from an experiment config file.

    Args:
        experiment_config_file: the path to an experiment config file that
          lists all parameters of an experiment

    Returns:
        a str representing the qdisc used in the experiment
    """

    experiment = experiment_util.from_config(experiment_config_file)
    return experiment.qdisc if experiment.qdisc else 'pfifo'


def parse_buffer_log_file(
        buffer_log_file: str,
        qdisc: str = 'pfifo',
        only_coexisting: bool = False,
        flow_start_times: list = [],
        flow_completion_times: list = [],
        ) -> list:
    """Parse a buffer log file and return the packet loss values measured
    in it.

    Args:
        buffer_log_file: the path to a buffer log file
        qdisc: the qdisc used in the experiment; if None, default to
          assuming it is pfifo and the qdisc has no parent
        only_coexisting: count loss only when flows coexist. This is for
          experiments where one or more long flows coexist with some short
          flows; one might want to count only the loss that happened
          during coexistence of long and short flows. If this option is
          True, you MUST provide the lists flow_start_times and
          flow_completion_times. This function will then count loss only
          from flow_start_times on for flow_completion_times long.
        flow_start_times: if only_coexisting, this is the list of start
          times for the flows during which loss should be counted, i.e.,
          the start times of the short flows
        flow_completion_times: if only_coexisting, this is the list of
          FCTs for the flows for which flow_start_times provides the start
          times

    Returns:
        a list of int values that represent the total amount of loss at
          each measuring interval, in packets
    """

    with open(buffer_log_file) as f:
        lines = f.readlines()

        loss_values = []
        cur_time = 0  # if only_coexisting, this tracks the current time
        cur_flow = 0  # if only_coexisting, this tracks the index of the
                      # current flow
        delta = 0     # if only_coexisting, this tracks the difference between
                      # the actual total loss and the loss only during times
                      # when flows coexisted

        # Only check the backlog for the qdisc that was used at the bottleneck
        # in the experiment.
        # That qdisc may have a parent, which has the same backlog values. We
        # only need the values once.
        # If no experiment_config_file was specified or the config does not
        # define a qdisc, we assume that the default qdisc is pfifo, and that
        # it has no parent qdisc.
        currently_parsing_qdisc = False
        for line in lines:
            if line.startswith('qdisc ' + qdisc):
                currently_parsing_qdisc = True
                continue
            elif line.startswith('qdisc '):  # Any other qdisc
                currently_parsing_qdisc = False

            if not currently_parsing_qdisc:
                continue

            stripped_line = line.strip()
            if stripped_line.startswith('Sent'):
                split = stripped_line.split()

                cur_loss_str = split[6]
                cur_loss = int(cur_loss_str.split(',')[0])

                if only_coexisting:
                    cur_flow_start = flow_start_times[cur_flow]
                    cur_flow_end = flow_start_times[cur_flow] + \
                            flow_completion_times[cur_flow]

                    # If flows are not coexisting at cur_time, track the delta
                    if cur_time <= cur_flow_start or cur_time > cur_flow_end:
                        delta += cur_loss - (loss_values[-1] + delta) \
                                if len(loss_values) > 1 else 0

                    cur_time += constants.BUFFER_LOGGING_FREQUENCY

                    if cur_time > cur_flow_end and \
                            cur_flow < len(flow_start_times) - 1:
                        cur_flow += 1

                # Apply the delta
                # If not only_coexisting, delta = 0, so this does nothing
                # Otherwise, it reduces the current loss count by the amount
                # of loss that happened during times when flows didn't coexist
                cur_loss -= delta

                loss_values.append(cur_loss)

        return loss_values


def plot_packet_loss(
        loss_values: list,
        output_file: str = None,
        ):
    """Plot the packet loss using raw data as input.

    Args:
        loss_values: a list that contains the packet loss values, i.e.,
          the total loss measured at each interval
        output_file: a path to a file to store the plot into; if None,
          display the plot directly
    """

    fig, ax = plt.subplots()

    y = loss_values
    x = [i * constants.BUFFER_LOGGING_FREQUENCY for i in range(len(y))]

    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Total packet loss [packets]')

    ax.grid(axis='y', visible=True)

    ax.plot(x, y)

    plt.tight_layout()

    if output_file:
        plt.savefig(output_file)
    else:
        plt.show()


def plot_packet_loss_from_files(
        buffer_log_file: str,
        output_file: str = None,
        experiment_config_file: str = None,
        ):
    """Plot the packet loss using the buffer log file (and optionally,
    the experiment config file) as input.

    Args:
        buffer_log_file: the file that contains the buffer log, as created
          by the experiment runner script
        output_file: a path to a file to store the plot into; if None,
          display the plot directly
        experiment_config_file: a path to an experiment config file to read
          the qdisc from
    """

    qdisc = get_qdisc(experiment_config_file) \
            if experiment_config_file else 'pfifo'
    loss_values = parse_buffer_log_file(buffer_log_file, qdisc)
    plot_packet_loss(loss_values, output_file)
    print('Total packet loss:', loss_values[-1])


def main():
    args = parse_args()
    buffer_log_file = args.file
    output_file = args.output if args.output else None
    experiment_config_file = args.experimentconfig \
            if args.experimentconfig else None

    plot_packet_loss_from_files(buffer_log_file, output_file,
            experiment_config_file)


if __name__ == '__main__':
    main()

