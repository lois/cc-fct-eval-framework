"""Server configuration constants.

Edit these when setting up the framework on a set of servers.
Change the IP addresses and interfaces based on your setup.
"""

# Public IP addresses
# These are used for the controller to connect to the other servers
PUBLIC_IPS_SENDERS = [
        '10.10.2.7',  # node14
        '10.10.2.8',  # node15
        '10.10.2.1',  # node18
        '10.10.2.9',  # node6
]
PUBLIC_IP_ROUTER =   '10.10.2.12'  # node7
PUBLIC_IP_RECEIVER = '10.10.2.3'  # node24

PUBLIC_IPS = [
        *PUBLIC_IPS_SENDERS,
        PUBLIC_IP_ROUTER,
        PUBLIC_IP_RECEIVER,
]

# Experiment IP addresses
# These are used within experiments; the servers that are directly
# connected are configured to use these IP addresses on the interfaces via
# which they are connected
EXPERIMENT_IPS_SENDERS = [
        '13.37.1.0',  # node14
        '13.37.1.1',  # node15
        '13.37.1.2',  # node18
        '13.37.1.3',  # node6
]
EXPERIMENT_IP_ROUTER = '13.37.2.0'  # node7
EXPERIMENT_IP_RECEIVER = '13.37.3.0'  # node24

EXPERIMENT_IPS = [
        *EXPERIMENT_IPS_SENDERS,
        EXPERIMENT_IP_ROUTER,
        EXPERIMENT_IP_RECEIVER,
]

# Interfaces
# These tell via which interface a server is connected to a neighbor
INTERFACES_ROUTER_TO_SENDERS = [
        'eth0',
        'eth1',
        'eth3',
        'eth5',
]
INTERFACE_ROUTER_TO_RECEIVER = 'eth2'

INTERFACES_SENDERS_TO_ROUTER = [
        'eth0',
        'eth2',
        'eth3',
        'eth5',
]

INTERFACE_RECEIVER_TO_ROUTER = 'eth2'

