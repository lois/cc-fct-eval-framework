"""This file contains functions for producing commands that can be sent to
the servers (senders, router, receiver), e.g. for configuring them or for
starting a sender or receiver process.
"""

import os

import constants


def start_receiver_fctm(
        duration: int,
        ip_receiver: str,
        quickack: int = 0,
        ) -> str:
    """Produce a command that starts an fctm receiver process.

    Args:
        duration: the duration for which the receiver should listen for
          connections in seconds
        ip_receiver: the IP address the server should use for the
          experiment; this should usually be EXPERIMENT_IP_RECEIVER
        quickack: whether TCP_QUICKACK should be enabled (1) or not (0)

    Returns:
        a string that starts the fctm receiver when executed as command
    """

    return '{} -s {} -i {} -q {}'.format(constants.FCTM_PATH, duration,
            ip_receiver, quickack)


def start_sender_fctm(
        flow_size: int,
        flow_number: int,
        ip_receiver: str,
        ) -> str:
    """Produce a command that starts an fctm sender process.

    Args:
        flow_size: the size of the flow to generate in packets
        flow_number: an integer that uniquely identifies this sender / flow
          within the context of its thread; i.e., sender 1 may send three
          flows: 0, 1, and 2; sender 2 may send two flows: 0, and 1. This
          flow_number is used to generate a unique file to write the flow's
          output into
        ip_receiver: the IP address of the receiver; this should usually
          be EXPERIMENT_IP_RECEIVER

    Returns:
        a string that starts the fctm sender when executed as command
    """

    result_file = constants.SENDER_RESULT_FILE_PREFIX_FLOW + str(flow_number)
    return '{} -c {} -i {} > {}'.format(
            constants.FCTM_PATH, flow_size, ip_receiver, result_file)


def start_receiver_pcp(
        duration: int,
        ):
    """Produce a command that start a pcp receiver process.

    Args:
        duration: the duration for which the receiver should listen for
          connections in seconds
    
    Returns:
        a string that starts the pcp receiver when executed as command
    """

    # "|| true" prevents a crash, as timeout yields a non-zero error code
    return 'timeout {} {} {} || true'.format(
            duration, constants.PCP_RECEIVER_PATH, constants.PCP_PORT)


def start_sender_pcp(
        flow_size_packets: int,
        flow_number: int,
        ip_receiver: str,
        executable: str,
        extra_args: str = '',
        ):
    """Produce a command that starts a pcp sender process with the
    specified executable as sender.

    Args:
        flow_size_packets: the size of the flow to generate in packets.
          This value will be converted to bytes in this function, as pcp
          senders expect bytes not packets
        flow_number: an integer that uniquely identifies this sender / flow
          within the context of its thread; i.e., sender 1 may send three
          flows: 0, 1, and 2; sender 2 may send two flows: 0, and 1. This
          flow_number is used to generate a unique file to write the flow's
          output into
        ip_receiver: the IP address of the receiver; this should usually
          be EXPERIMENT_IP_RECEIVER
        executable: the name of the executable to use as a sender process;
          this should usually be given by the experiment config as CCA
        extra_args: additional arguments that should be provided to the
          executable (will be added at the end of the args)

    Returns:
        a string that starts the pcp sender when executed as command
    """

    flow_size_bytes = flow_size_packets * constants.TCP_PACKET_SIZE
    sender_path = os.path.join(constants.PCP_BASE_PATH, executable)
    result_file = constants.SENDER_RESULT_FILE_PREFIX_FLOW + str(flow_number)
    return '{} {} {} {} 1 0.000001 {} > {}'.format(
            sender_path, ip_receiver, constants.PCP_PORT, flow_size_bytes,
            extra_args, result_file)


def qdisc_add(device: str, config: str) -> str:
    """Produce a tc command that adds a qdisc to an interface.

    Args:
        device: the interface to configure, e.g. "eth0"
        config: the actual qdisc and optinally configuration to do,
          e.g. "pfifo limit 120"

    Returns:
        a string that adds the qdisc when executed as command
    """

    return 'sudo tc qdisc add dev {} root {}'.format(device, config)


def qdisc_del(device: str) -> str:
    """Produce a tc command that deletes a qdisc from an interface.

    Args:
        device: the interface from which the qdisc should be deleted, e.g.
          "eth0"

    Returns:
        a string that deletes a qdisc when executed as command
    """

    return 'sudo tc qdisc delete dev {} root'.format(device)


def netem_add(device: str, config: str) -> str:
    """Produce a netem command that adds a config to an interface.

    Args:
        device: the interface to configure, e.g. "eth0"
        config: the actual configuration to do, e.g. "delay 20ms"

    Returns:
        a string that adds the netem config when executed as command
    """

    netem_config = 'netem {}'.format(config)
    return qdisc_add(device, netem_config)


def set_bandwidth(device: str, bw: int) -> str:
    """Produce an ethtool command that sets the bandwidth on an interface.

    Args:
        device: the interface to set bandwidth on, e.g. "eth0"
        bw: the bandwidth as integer in mbit

    Returns:
        a string that sets the bandwidth when executed as command
    """

    return 'sudo ethtool -s {} speed {}'.format(device, bw)


def set_cca(congestion_control_algorithm: str) -> str:
    """Produce a sysctl command that sets the congestion control algorithm
    (CCA) to use.
    
    Args:
        congestion_control_algorithm: the CCA to use

    Returns:
        a string that set the CCA when executed as command
    """

    return 'sudo sysctl -w net.ipv4.tcp_congestion_control={} -q'.format(
            congestion_control_algorithm)


def set_initial_window(
        device: str,
        ip_receiver: str,
        ip_router: str,
        iw: int,
        ) -> str:
    """Produce an ip route command that sets the initial window value for
    connections to a receiver on an interface.

    Args:
        device: the interface on which connections go out, e.g. "eth0"
        ip_receiver: a string representation of the receiver's IP address
          (this should usually be EXPERIMENT_IP_RECEIVER)
        ip_router: a string representation of the router's IP address
          (this should usually be EXPERIMENT_IP_ROUTER)
        iw: the initial window value to use

    Returns:
        a string that sets the initial window when executed as command
    """

    return 'sudo ip r change {} via {} dev {} initcwnd {}'.format(
            ip_receiver, ip_router, device, iw)


def start_udp_receiver(duration: int) -> str:
    """Produce an iperf3 command that runs a server / listener for a certain
    duration; to be used for a UDP traffic receiver.

    Args:
        duration: the duration in seconds for which the receiver should
          listen for traffic; this should usually be slightly longer than
          the experiment duration

    Returns:
        a string that starts an iperf3 server when executed as command
    """

    # We use "|| true" to avoid fabric / paramiko complaining about a non-zero
    # return code (timeout generates a non-zero return code)
    return 'timeout {} iperf3 -s || true'.format(duration)


def start_udp_sender(
        ip_receiver: str,
        bandwidth: str,
        duration: int,
        ) -> str:
    """Produce an iperf3 command that runs a client that send UDP traffic.
    
    Args:
        ip_receiver: a string representation of the receiver's IP address
          (this should usually be EXPERIMENT_IP_RECEIVER)
        bandwidth: a string that defines the bandwidth with which the sender
          should send UDP traffic, in the format of iperf (e.g. "15m")
        duration: the duration in seconds for which the sender should send
          traffic; this should usually be the experiment duration

    Returns:
        a string that starts an iperf3 UDP client when executed as command
    """

    return 'iperf3 -c {} -u -b {} -t {} || true'.format(ip_receiver, bandwidth,
            duration)


def start_measuring_socket_stats(duration: int) -> str:
    """Produce a command that measures socket stats at the specified
    interface for a given duration (using ss).

    Args:
        duration: the duration in seconds for which you want to measure
          socket stats

    Returns:
        a string that starts measuring socket stats using ss and writes
          the output into a file when executed as a command
    """

    log_count = duration * int(1 / constants.SS_LOGGING_FREQUENCY)
    return '{} {} {} ss -tin > {}'.format(
            constants.LOOPPL_PATH,
            constants.SS_LOGGING_FREQUENCY, log_count,
            constants.SS_LOG_FILE_NAME)


def start_measuring_buffer_log(intfc: str, duration: int) -> str:
    """Produce a command that measures the buffer backlog at the specified
    interface for a given duration.

    Args:
        intfc: the interface on which you want to measure the buffer log;
          this should usually be INTERFACE_ROUTER_TO_RECEIVER
        duration: the duration in seconds for which you want to measure the
          buffer log

    Returns:
        a string that starts measuring the buffer backlog using tc and
          writes the output into a file when executed as command
    """

    log_count = duration * int(1 / constants.BUFFER_LOGGING_FREQUENCY)
    return '{} {} {} tc -s qdisc show dev {} > {}'.format(
            constants.LOOPPL_PATH,
            constants.BUFFER_LOGGING_FREQUENCY, log_count, intfc,
            constants.BUFFER_LOG_FILE_NAME)

