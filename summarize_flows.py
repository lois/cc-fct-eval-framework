#!/usr/bin/env python3

"""Functions for summarizing information about flows, i.e., their FCTs,
slowdown, etc.
"""

import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import compute_slowdown
import experiment_util
import constants


# REGULAR FONT SIZE #
#plt.rcParams.update({'font.size': 12})
# PAPER FONT SIZE #
plt.rcParams.update({'font.size': 16})


def parse_args():
    """Parse command line arguments.

    Returns:
        the args Namespace from argparse
    """

    parser = argparse.ArgumentParser(description='Find all FCT result ' + \
            'files in an experiment\'s result directory and create a ' + \
            'slowdown summary as well as a plot that shows the flows over ' + \
            'time.')

    parser.add_argument('directory', help='the path to the directory that ' + \
            'contains the experiment result files')

    return parser.parse_args()


def find_fct_files(results_directory: str) -> list:
    """Find the FCT result files in the results directory and return a list
    of paths to them.

    Args:
        results_directory: the path to the results directory

    Returns:
        a list of FCT result files
    """

    files = []

    for f in os.listdir(results_directory):
        if f.startswith(constants.SENDER_RESULT_FILE_PREFIX_SENDER):
            files.append(f)

    return files


def parse_flow_start_times_file(flow_start_times_file: str) -> list:
    """Parse a flow start times file and return a list[list[float]] that
    contains for every sender a list of its flow start times.

    Args:
        flow_start_times_file: the path to the flow start times file

    Returns:
        a list that contains for every sender a list of its flow start times
    """

    with open(flow_start_times_file) as f:
        lines = f.readlines()

        flow_start_times = []
        sender_index = -1

        for line in lines:
            if line.startswith('Sender'):
                flow_start_times.append([])
                sender_index += 1
                continue
            else:
                start_time = float(line)
                flow_start_times[sender_index].append(start_time)

        return flow_start_times


def create_slowdown_summary(
        fct_files: list,
        flow_start_times_file: str,
        experiment_config_file: str,
        results_directory: str,
        ) -> list:
    """Create a slowdown summary that contains information about the flows:
    their start time, FCT, size, and slowdown.

    Args:
        fct_files: a list of names of FCT result files
        flow_start_times_file: the path to the flow start times file
        experiment_config_file: the path to the experiment config file
        results_directory: the path to the results directory

    Returns:
        the slowdown summary as a list that contains for each sender a list
          of tuples where a tuple is the infomation about one flow (flow
          start time, FCT, flow size, optimal FCT, slowdown)
    """

    flow_start_times = parse_flow_start_times_file(flow_start_times_file)

    # Slowdown summary is a list[list[tuple()]]:
    # for each sender: for each flow: a tuple of
    # (flow start time, FCT, flow size, optimal FCT, slowdown)
    slowdown_summary = []
    sender_index = 0
    flow_index = -1
    for fct_file in sorted(fct_files):
        if not 'fct' in fct_file:  # could also be ss log
            continue

        fct_file = os.path.join(results_directory, fct_file)
        fct, flow_size = compute_slowdown.parse_fct_file(fct_file)

        if sender_index == \
                compute_slowdown.get_sender_index_from_fct_file_name(fct_file):
            flow_index += 1
        else:
            flow_index = 0
            sender_index += 1

        start_time = flow_start_times[sender_index][flow_index]
        slowdown, optimal_fct = compute_slowdown.compute_slowdown_from_files(
                fct_file, experiment_config_file)

        # If we did not add results for this sender to the summary previously,
        # create a new list for that sender index first
        if len(slowdown_summary) <= sender_index:
            slowdown_summary.append([])

        slowdown_tuple = (start_time, fct, flow_size, optimal_fct, slowdown)
        slowdown_summary[sender_index].append(slowdown_tuple)

    return slowdown_summary


def slowdown_summary_as_string(slowdown_summary: list) -> str:
    """Write a slowdown summary nicely as string.

    Args:
        slowdown_summary: a slowdown summary as created by
          create_slowdown_summary()

    Returns:
        a string that summarizes the slowdown summary in a nice format
    """

    summary = ''

    for i, sender in enumerate(slowdown_summary):
        summary += 'Sender {}\n\n'.format(i)
        for j, flow in enumerate(sender):
            summary += 'Flow {}: '.format(j)
            summary += 'start={}, fct={}s, size={}, optimal_fct={}s, ' \
                    'slowdown={}\n'.format(flow[0], flow[1], flow[2], flow[3],
                    flow[4])
        summary += '\n'

    return summary


def get_flow_bar_y_values(flow_tuples: list) -> list:
    """Compute the y values for the flow bars in a flow summary plot.

    This makes sure that no bars overlap such that each flow can be viewed
    individually.

    Args:
        flow_tuples: a list of flow tuples that contain information about
          the flow start times and FCTs

    Returns:
        a list of int values that define for each flow its y position in
          the flow summary plot
    """

    y_values = [0] * len(flow_tuples)  # default for every flow: y = 0

    # Iterate flow tuples
    for i, tupi in enumerate(flow_tuples):
        start_time_i = tupi[0]
        fct_i = tupi[1]
        end_time_i = start_time_i + fct_i

        # Check all ealier and later flows for overlaps
        for j, tupj in enumerate(flow_tuples):
            start_time_j = tupj[0]
            fct_j = tupj[1]
            end_time_j = start_time_j + fct_j

            if j < i:  # earlier flows
                # Overlap?
                if start_time_i < end_time_j and y_values[i] == y_values[j]:
                    # Move y of later flow (i) up
                    y_values[i] += 1
            elif j > i:  # later flows
                # Overlap?
                if end_time_i > start_time_j and y_values[i] == y_values[j]:
                    # Move y of later flow (j) up
                    y_values[j] += 1

    return y_values


def plot_flow_summary(
        slowdown_summary: list,
        experiment_duration: int,
        output_file: str = None,
        ):
    """Create a flow summary plot.

    This plot shows a bar for each flow to show its start time and FCT and
    colors the bars according to the flow's slowdown.

    Args:
        slowdown_summary: a slowdown summary as created by
          create_slowdown_summary()
        experiment_duration: the experiment duration in seconds
    """

    flow_tuples = []
    for sender in slowdown_summary:
        for tup in sender:
            flow_tuples.append(tup)

    # Sort flow tuples by start times
    flow_tuples = sorted(flow_tuples, key=lambda x: x[0])

    start_times = [tup[0] for tup in flow_tuples]
    fcts = [tup[1] for tup in flow_tuples]
    sizes = [tup[2] for tup in flow_tuples]
    optimal_fcts = [tup[3] for tup in flow_tuples]
    slowdowns = np.array([tup[4] for tup in flow_tuples])
    y = get_flow_bar_y_values(flow_tuples)

    fig, ax = plt.subplots()

    worst_slowdown = 10
    normalized_slowdowns = slowdowns / worst_slowdown
    #colors = cm.afmhot_r(normalized_slowdowns)
    colors = cm.Reds(normalized_slowdowns)

    # Fake plot for making a colorbar
    colorplot = ax.scatter(slowdowns, slowdowns,
            c=np.linspace(1, worst_slowdown, num=len(slowdowns)), cmap=cm.Reds)
    plt.cla()

    ax.set_xlabel('Time [s]')
    
    ax.set_xlim([0, experiment_duration])
    ax.set_ylim(0, 10)
    ax.set_yticks([])  # Hide ticks on y-axis

    plt.tight_layout()

    ax.barh(y, height=1, align='edge',
             left=start_times, width=fcts,
             edgecolor='black', color=colors)
    # TODO It would be nice to also show the optimal FCT as a line inside the bars

    cbaxes = inset_axes(ax, width='30%', height='5%', loc='upper right')
    cbticks = [1, worst_slowdown]
    cbar = plt.colorbar(colorplot, cax=cbaxes, orientation='horizontal',
                        ticks=cbticks)
    cbar.set_label('Slowdown')

    if output_file:
        plt.savefig(output_file)
    else:
        plt.show()


def summarize_flows_from_results_directory(
        results_directory: str,
        slowdown_summary_file: str,
        flow_summary_plot_file: str,
        ):
    """Create a slowdown summary and write it into a file and create a flow
    summary plot.

    Args:
        results_directory: the path to the results directory that contains
          all the result files from an experiment
        slowdown_summary_file: the path to the file in which you want to
          write the slowdown summary
        flow_summary_plot_file: the path to the file in which you want to
          store the flow summary plot
    """

    fct_files = find_fct_files(results_directory)
    flow_start_times_file = os.path.join(results_directory,
            constants.FLOW_START_TIMES_FILE_NAME)
    experiment_config_file = os.path.join(results_directory,
            constants.EXPERIMENT_CONFIG_FILE_NAME)

    slowdown_summary = create_slowdown_summary(fct_files, flow_start_times_file,
            experiment_config_file, results_directory)

    with open(slowdown_summary_file, 'w') as f:
        f.write(slowdown_summary_as_string(slowdown_summary))

    output_file = os.path.join(results_directory,
            constants.FLOW_SUMMARY_PLOT_FILE_NAME)

    experiment = experiment_util.from_config(experiment_config_file)
    plot_flow_summary(slowdown_summary, experiment.duration, output_file)


def main():
    args = parse_args()
    results_directory = args.directory

    slowdown_summary_file = os.path.join(results_directory,
            constants.SLOWDOWN_SUMMARY_FILE_NAME)
    flow_summary_plot_file = os.path.join(results_directory,
            constants.FLOW_SUMMARY_PLOT_FILE_NAME)

    summarize_flows_from_results_directory(results_directory,
            slowdown_summary_file, flow_summary_plot_file)


if __name__ == '__main__':
    main()

