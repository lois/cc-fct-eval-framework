#!/usr/bin/perl

# Call a command at a precise interval for a certain duration
# This script has very high precision and can, for instance, call a command
# every millisecond. Other solutions such as bash for + sleep incur additional
# delay and are, thus, not as precise.
#
# Usage:
# ./loop.pl interval totalsteps command
#   interval is the interval in s (after what time to call the command again)
#   totalsteps is the total number of steps for which this script should loop
#     (for some duration in s, totalsteps should be duration * 1 / interval)
#   command is the command you want to execute
#
# Example:
# ./loop.pl 0.001 10000 tc -s qdisc show dev eth0 > tc-stats.log
# to show tc stats every 0.001s (every 1ms) for 10k steps (10s in total)

use strict;
use warnings;
use Time::HiRes qw/time sleep/;

sub launch {
    return if fork;
    exec @_;
    die "Couldn't exec";
}

$SIG{CHLD} = 'IGNORE';

my $interval = shift;  # in s
my $totalsteps = shift;  # how often we should loop
                         # (should be duration * 1 / interval)
my $start = time();
for (my $i=0; $i < $totalsteps; $i++) {
    launch(@ARGV);
    $start += $interval;
    # safeguard although $start - time() should really rarely be < 0
    if ($start - time() > 0) {
        sleep $start - time();
    }
}
