"""Various constants."""

# Constant sizes we use

TCP_PACKET_SIZE = 1448  # bytes

TCP_HEADER_SIZE = 32  # bytes (20 (regular) + 12 (options used by Linux))
IP_HEADER_SIZE = 20  # bytes
ETHERNET_HEADER_SIZE = 14  # bytes


# Unit conversions

MILLISECONDS_TO_SECONDS = 1 / 1000
MICROSECONDS_TO_SECONDS = 1 / (1000 * 1000)

MBITS_TO_BITS = 1000 * 1000
BITS_PER_BYTE = 8


# Path at which the flow completion measuring tool (fctm) can be found on all
# senders and the receiver
FCTM_PATH = 'fctm/fctm'

# Path at which the PCP user space code can be found on all senders and the
# receiver
# In this directory, there should be executables for different CCAs, which can
# be used by specifying them as congestion_control_algorithms in an experiment
# config and setting pcp as executable in the config
# (e.g., pcp, js, hb)
PCP_BASE_PATH = 'pcp/pcp-ulevel/'

# Path at which the PCP receiver tool can be found
PCP_RECEIVER_PATH = PCP_BASE_PATH + 'server'

# Default port to use for PCP
PCP_PORT = 1338

# Strings for FCTM / PCP executables
STR_FCTM = 'fctm'
STR_PCP = 'pcp'


# How often should the router measure the buffer backlog, in seconds?
BUFFER_LOGGING_FREQUENCY = 0.01

# How often should the senders measure socket stats, in seconds?
SS_LOGGING_FREQUENCY = 0.01

# Path at which the loop perl script (loop.pl) can be found on the servers
LOOPPL_PATH = './loop.pl'


FORCED_FLOW_INITIAL_FLOW_NUMBER = 1000


# Experiment file names

# Each sender dumps the result of one flow into a file called
# SENDER_RESULT_FILE_PREFIX_FLOW + j
# where j is the flow number.
# On the controller, these files get stored into the results directory as
# SENDER_RESULT_FILE_PREFIX_SENDER + i + '_' +
#         SENDER_RESULT_FILE_PREFIX_FLOW + j
# where i is the sender number and j is the flow number.
SENDER_RESULT_FILE_PREFIX_SENDER = 'sender'
SENDER_RESULT_FILE_PREFIX_FLOW = 'fct_eval_results_flow_'

# The controller writes the flow start times into this file
FLOW_START_TIMES_FILE_NAME = 'flow_start_times'

# The router regularly dumps the buffer backlog into this file
BUFFER_LOG_FILE_NAME = 'buffer_log'

# Socket stats are regularly dumped into this file
SS_LOG_FILE_NAME = 'ss_log'

# The experiment config gets stored into this file
EXPERIMENT_CONFIG_FILE_NAME = 'experiment_config'

# The random seed gets stores into this file
RANDOM_SEED_FILE_NAME = 'random_seed'


# Analysis file names

SLOWDOWN_SUMMARY_FILE_NAME = 'slowdown_summary.txt'
METRICS_SUMMARY_FILE_NAME = 'metrics_summary.txt'
FLOW_SUMMARY_PLOT_FILE_NAME = 'flow_summary.pdf'

BUFFER_PLOT_FILE_NAME = 'buffer_log.pdf'

LOSS_PLOT_FILE_NAME = 'packet_loss.pdf'


# Defaults

DEFAULT_RESULTS_DIR = 'results'

DEFAULT_QDISC = 'pfifo'

