#!/usr/bin/env python3

"""Functions for plotting the buffer backlog over time.
"""

import argparse
import matplotlib.pyplot as plt

import experiment_util
import constants


def parse_args():
    """Parse command line arguments.

    Returns:
        the args Namespace from argparse
    """

    parser = argparse.ArgumentParser(description='Parse a buffer log file ' + \
            'and create a plot showing the buffer backlog over time.')

    parser.add_argument('file', help='path to the buffer log file')
    parser.add_argument('-o', '--output', help='path to a file to store ' + \
            'plot into; if omitted, show plot directly')
    parser.add_argument('-e', '--experimentconfig', help='path to the ' + \
            'experiment config file (useful for reading the buffer size ' + \
            'in order to display it in the plot)')

    return parser.parse_args()


def get_qdisc(experiment_config_file: str) -> str:
    """Obtain the qdisc from an experiment config file.

    Args:
        experiment_config_file: the path to an experiment config file that
          lists all parameters of an experiment

    Returns:
        a str representing the qdisc used in the experiment
    """

    experiment = experiment_util.from_config(experiment_config_file)
    return experiment.qdisc if experiment.qdisc else 'pfifo'


def get_buffer_size(experiment_config_file: str) -> int:
    """Obtain the buffer size (= queue size) from an experiment config file.

    Args:
        experiment_config_file: the path to an experiment config file that
          lists all parameters of an experiment

    Returns:
        an int representing the buffer size used in the experiment
    """

    experiment = experiment_util.from_config(experiment_config_file)
    return experiment.queue_size


def parse_buffer_log_file(buffer_log_file: str, qdisc: str = 'pfifo') -> list:
    """Parse a buffer log file and return the buffer backlog values measured
    in it.

    Args:
        buffer_log_file: the path to a buffer log file
        qdisc: the qdisc used in the experiment; if None, default to
          assuming it is pfifo and the qdisc has no parent

    Returns:
        a list of int values that represent the buffer backlog at each
          measuring interval, in packets
    """

    with open(buffer_log_file) as f:
        lines = f.readlines()

        backlog_values = []

        # Only check the backlog for the qdisc that was used at the bottleneck
        # in the experiment.
        # That qdisc may have a parent, which has the same backlog values. We
        # only need the values once.
        # If no experiment_config_file was specified or the config does not
        # define a qdisc, we assume that the default qdisc is pfifo, and that
        # it has no parent qdisc.
        currently_parsing_qdisc = False
        for line in lines:
            if line.startswith('qdisc ' + qdisc):
                currently_parsing_qdisc = True
                continue
            elif line.startswith('qdisc '):  # Any other qdisc
                currently_parsing_qdisc = False

            if not currently_parsing_qdisc:
                continue

            stripped_line = line.strip()
            if stripped_line.startswith('backlog'):
                split = stripped_line.split()

                cur_packets_str = split[2]
                cur_packets_backlog = int(cur_packets_str.split('p')[0])

                backlog_values.append(cur_packets_backlog)

        return backlog_values


def plot_buffer_log(
        buffer_log: list,
        output_file: str = None,
        buffer_size: int = None,
        ):
    """Plot the buffer backlog using raw data as input.

    Args:
        buffer_log: a list that contains the buffer log, i.e., the buffer
          backlog measured at each interval
        output_file: a path to a file to store the plot into; if None,
          display the plot directly
        buffer_size: the maximum buffer size; if not None, it gets shown in
          the plot
    """

    fig, ax = plt.subplots()

    y = buffer_log
    x = [i * constants.BUFFER_LOGGING_FREQUENCY for i in range(len(y))]

    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Buffer Backlog [packets]')

    ax.grid(axis='y', visible=True)

    ax.plot(x, y)

    if buffer_size:
        bufsize_y = [buffer_size] * len(x)
        ax.plot(x, bufsize_y, color='black')

    plt.tight_layout()

    if output_file:
        plt.savefig(output_file)
    else:
        plt.show()


def plot_buffer_log_from_files(
        buffer_log_file: str,
        output_file: str = None,
        experiment_config_file: str = None,
        ):
    """Plot the buffer backlog using the buffer log file (and optionally,
    the experiment config file) as input.

    Args:
        buffer_log_file: the file that contains the buffer log, as created
          by the experiment runner script
        output_file: a path to a file to store the plot into; if None,
          display the plot directly
        experiment_config_file: a path to an experiment config file to read
          the buffer size from; if None, do not show the buffer size in the
          plot
    """

    qdisc = get_qdisc(experiment_config_file) \
            if experiment_config_file else 'pfifo'
    buffer_log = parse_buffer_log_file(buffer_log_file, qdisc)
    buffer_size = get_buffer_size(experiment_config_file) \
            if experiment_config_file else None
    plot_buffer_log(buffer_log, output_file, buffer_size)


def main():
    args = parse_args()
    buffer_log_file = args.file
    output_file = args.output if args.output else None
    experiment_config_file = args.experimentconfig \
            if args.experimentconfig else None

    plot_buffer_log_from_files(buffer_log_file, output_file,
            experiment_config_file)


if __name__ == '__main__':
    main()

